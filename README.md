# Book Exchange App built with Flutter and Firebase

[https://www.bookexchange.app](https://www.bookexchange.app)


![](assets/Icon.png =50x50)

##A book exchange app built with Flutter and Firebase
***
  
####Authentication with Firebase Authentication
####Retrieve user's profile picture from Firebase Storage
![Alt text](assets/gif/login.gif)
***

####Add new book by searching a book's ISBN13 via Google Books API
####Add new book into Firestore Document Collection
####Allow changes only by user such as deleting a book and listing book in public
![Alt text](assets/gif/add_book.gif) 
***

####Apply rules in Firestore to only display books that are listed as public by user
![Alt text](assets/gif/discover.gif) 


