//import 'package:flutter/material.dart';
//import 'package:flutter_test/flutter_test.dart';
//
//import '../lib/login_page.dart';
//
//class AuthMock implements Auth {
//  AuthMock({this.userId});
//  String userId;
//  bool didRequestSignIn = false;
//  Future<String> signIn(String email, String password) async {
//    didRequestSignIn = true;
//    if (userId != null) {
//      return Future.value(userId);
//    } else {
//      throw StateError('No user');
//    }
//  }
//}
//
//void main() {
//  testWidgets(
//      'non-empty email and password, valid account, calls sign in, succeeds',
//      (WidgetTester tester) async {
//    // mock with a user id - simulates success
//    AuthMock mock = new AuthMock(userId: 'uid');
//    LoginPage loginPage = new LoginPage(title: 'test', auth: mock);
//    await tester.pumpWidget(buildTestableWidget(loginPage));
//
//    Finder emailField = find.byKey(new Key('email'));
//    await tester.enterText(emailField, 'email');
//
//    Finder passwordField = find.byKey(new Key('password'));
//    await tester.enterText(passwordField, 'password');
//
//    Finder loginButton = find.byKey(new Key('login'));
//    await tester.tap(loginButton);
//
//    await tester.pump();
//
//    Finder hintText = find.byKey(new Key('hint'));
//    expect(hintText.toString().contains('Signed In'), true);
//
//    expect(mock.didRequestSignIn, true);
//  });
//
//  testWidgets(
//      'non-empty email and password, invalid account, calls sign in, fails',
//      (WidgetTester tester) async {
//    // mock without user id - throws an error and simulates failure
//    AuthMock mock = new AuthMock(userId: null);
//    LoginPage loginPage = new LoginPage(title: 'test', auth: mock);
//    await tester.pumpWidget(buildTestableWidget(loginPage));
//
//    Finder emailField = find.byKey(new Key('email'));
//    await tester.enterText(emailField, 'email');
//
//    Finder passwordField = find.byKey(new Key('password'));
//    await tester.enterText(passwordField, 'password');
//
//    Finder loginButton = find.byKey(new Key('login'));
//    await tester.tap(loginButton);
//
//    await tester.pump();
//
//    Finder hintText = find.byKey(new Key('hint'));
//    expect(hintText.toString().contains('Sign In Error'), true);
//
//    expect(mock.didRequestSignIn, true);
//  });
//}
