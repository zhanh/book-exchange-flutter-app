//import 'package:flutter/material.dart';
//
//import 'auth.dart';
//import 'backdrop_menu.dart';
//import 'colors.dart';
//import 'discover.dart';
//import 'home.dart';
//import 'my_library.dart';
//
////
////class RoutePages extends StatefulWidget {
////
////
////
////  @override
////  State<StatefulWidget> createState() => new _RoutePagesState();
////}
////
//////enum PageId { home, my_library, wishlist, discover }
//
//class RoutePages extends StatefulWidget {
//  RoutePages({this.auth, this.onSignedOut});
//
//  final BaseAuth auth;
//  final VoidCallback onSignedOut;
//
//  @override
//  RoutePagesState createState() {
//    return new RoutePagesState();
//  }
//}
//
//class RoutePagesState extends State<RoutePages> {
//  int currentTab = 0;
//  List<StatefulWidget> frontPages = [
//    Home(),
//    MyLibrary(),
//    MyLibrary(),
//    Discover()
//  ];
//
//  List<String> frontPagesTitle = ["HOME", "MY LIBRARY", "WISHLIST", "DISCOVER"];
//  Widget currentPage;
//  String currentPageTitle;
//
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    currentPage = frontPages[currentTab];
//    currentPageTitle = frontPagesTitle[currentTab];
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      backgroundColor: kPrimaryColorAmber,
//      bottomNavigationBar: BottomNavigationBar(
//        currentIndex: currentTab,
//        onTap: (int index) {
//          print("HELLO");
//          currentTab = index;
//          print('currentIndex =  ${currentTab.toString()}');
//
//          setState(() {
//            currentPage = frontPages[currentTab];
//            currentPageTitle = frontPagesTitle[currentTab];
//            print("CURRENTPAGE: ${currentPage}");
//          });
////          Navigator.of(context).pushNamed('/${currentPage}');
//        },
//        type: BottomNavigationBarType.fixed,
//        items: [
//          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('HOME')),
//          BottomNavigationBarItem(
//              icon: Icon(Icons.folder_special), title: new Text('MY LIBRARY')),
//          BottomNavigationBarItem(
//              icon: Icon(Icons.favorite), title: Text('WISHLIST')),
//          BottomNavigationBarItem(
//              icon: Icon(Icons.search), title: Text('DISCOVER')),
//        ],
//      ),
//      body: Scaffold(
//        body: (FutureBuilder<BackdropMenu>(builder: (context, snapshot) {
//          if (snapshot.hasData) {
//            return BackdropMenu(
//              frontLayer: currentPage,
//              frontTitle: Text(currentPageTitle),
//              auth: this.widget.auth,
//              onSignedOut: this.widget.onSignedOut,
//            );
//          }
//        })),
//      ),
//    );
//  }
//}
