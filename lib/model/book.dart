class Book {
  final String title;
  final String author;
  final String isbn13;
  final String smallThumbnailUrl;
  final String thumbnailUrl;
//  final String thumbnail;
//  final String categories;

  Book(
      {this.title,
      this.author,
      this.isbn13,
      this.smallThumbnailUrl,
      this.thumbnailUrl
//      this.thumbnail,
//      this.categories
      });

  factory Book.fromJson(Map<String, dynamic> json) {
    print("IN JSON");
    return Book(
      title: json['items'][0]['volumeInfo']['title'] as String,
      author: json['items'][0]['volumeInfo']['authors'][0],
      isbn13: json['items'][0]['volumeInfo']['industryIdentifiers'][1]
              ['identifier']
          .toString(),
      smallThumbnailUrl: json['items'][0]['volumeInfo']['imageLinks']
          ['smallThumbnail'] as String,
      thumbnailUrl:
          json['items'][0]['volumeInfo']['imageLinks']['thumbnail'] as String,
//            ['smallThumbnail'],
//        thumbnail: json['items']['volumeInfo']['imageLinks']['thumbnail'],
//        categories: json['items']['volumeInfo']['categores']);
    );
  }
}
