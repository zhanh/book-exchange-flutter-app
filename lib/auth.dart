import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';

abstract class BaseAuth {
  Future<String> signInWithEmailAndPassword(String email, String password);

  Future<String> createUserWithEmailAndPassword(String email, String password);

  Future<FirebaseUser> currentUser();

//  String uid();
  Future<void> signOut();

//  set assignUserId(FirebaseUser user);
//  get retrieveUserId;
//  Future<String> createUser(String email, String password);
//  Future<void> signOut();
  String retrieveUserId();
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  String userId;

  String retrieveUserId() {
    print("USER ID IN AUTH: $userId.toString");
    return userId;
  }

  Future<String> signInWithEmailAndPassword(
      String email, String password) async {
    FirebaseUser user = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    userId = user.uid;
    return userId;
  }

  Future<String> createUserWithEmailAndPassword(
      String email, String password) async {
    FirebaseUser user = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    userId = user.uid;
    return userId;
  }

  Future<FirebaseUser> currentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
//    assignUserId = user;

    return user;
  }

//
//  String uid() {
//    print("CURR USER UID IN AUTH: $userId.toString");
//    return userId.toString();
//  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }

//  Future<String> createUser(String email, String password) async {
//    FirebaseUser user = await _firebaseAuth.createUserWithEmailAndPassword(
//        email: email, password: password);
//    return user.uid;
//  }
//
//  Future<String> currentUser() async {
//    FirebaseUser user = await _firebaseAuth.currentUser();
//    return user != null ? user.uid : null;
//  }
//
//  Future<void> signOut() async {
//    return _firebaseAuth.signOut();
//  }
}
