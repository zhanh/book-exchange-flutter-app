import 'package:flutter/material.dart';

import 'auth.dart';
import 'colors.dart';

class LoginPage extends StatefulWidget {
  final BaseAuth auth;
  final VoidCallback onSignedIn;

  LoginPage({this.auth, this.onSignedIn});

  @override
  _LoginPageState createState() => new _LoginPageState();
}

enum FormType { login, register }

class _LoginPageState extends State<LoginPage> {
  static final formKey = new GlobalKey<FormState>();

  String _email;
  String _password;
  FormType _formType = FormType.login;

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      print("Valid form: ${_email.toString()}, ${_password.toString()}");
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      try {
        if (_formType == FormType.login) {
          String userId =
              await widget.auth.signInWithEmailAndPassword(_email, _password);
          print("Signed In: ${userId.toString()}");
        } else {
          String userId = await widget.auth
              .createUserWithEmailAndPassword(_email, _password);
          print(
              'Register user: ${userId.toString()}, ${_email.toString()}, ${_password.toString()}');
        }
//        print("WIDGET.onSignedIN is called");
        widget.onSignedIn();
      } catch (e) {
//        print('Error: ${e.toString()}');
      }
    }
  }

  void moveToRegister() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.register;
    });
  }

  void moveToLogin() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.login;
    });
  }

//
//  Widget buildHintText() {
//    return new Text(
//      _authHint,
//      key: new Key('hint'),
//      style: new TextStyle(fontSize: 14.0, color: kPrimaryDarkColorBrown),
//      textAlign: TextAlign.center,
//    );
//  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new Scaffold(
      //avoid squishing when keyboard is pushed
      resizeToAvoidBottomPadding: false,
      backgroundColor: kPaperWhite,
      body: new SafeArea(
        child: new ListView(
          padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 30.0),
          children: <Widget>[
            const SizedBox(height: 20.0),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Image.asset('assets/Icon.png'),
                  height: 80.0,
                  width: 80.0,
//                  color: Colors.black45,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 5.0,
                          color: Colors.black12,
//                          offset: Offset(0.0, 4.0),
                          spreadRadius: 1.0)
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(height: 8.0),
            const Text(
              'BOOK EXCHANGE',
              style: TextStyle(color: kSecondaryDarkColorBrown),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 20.0),
            Form(
                key: formKey,
                child: Column(
                  children: buildInputs() + buildSubmitButtons(),
                ))
          ],
        ),
      ),
    );
  }

  List<Widget> buildInputs() {
    return [
      TextFormField(
        keyboardType: TextInputType.emailAddress,
        style: TextStyle(color: kSecondaryDarkColorBrown, fontSize: 14.0),
        decoration: InputDecoration(
          labelText: 'Email Address',
        ),
        validator: (value) =>
            value.isEmpty ? 'Please enter a valid email address.' : null,
        onSaved: (value) => _email = value,
      ),
      SizedBox(
        height: 12.0,
      ),
      // [Password]
      TextFormField(
        obscureText: true,
        style: TextStyle(color: kSecondaryDarkColorBrown, fontSize: 14.0),
        decoration: InputDecoration(labelText: 'Password'),
        validator: (val) =>
            val.isEmpty ? 'Password is invalid, please try again.' : null,
        onSaved: (value) => _password = value,
      ),
      SizedBox(
        height: 12.0,
      ),
    ];
  }

  List<Widget> buildSubmitButtons() {
    print('FORMTYPE: ${ _formType.toString()}');

    if (_formType == FormType.login) {
      return [
        // TODO: Add button bar (101)
        Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 12.0),
                child: RaisedButton(
                  child: Text('LOGIN'),
                  shape: BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  ),
                  elevation: 6.0,
                  onPressed: () {
                    validateAndSubmit();
                    //pop the most recent route from the Navigator
//                        Navigator.pop(context);
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 1.0,
                      width: 120.0,
                      child: Center(
                        child: Container(
                          margin: new EdgeInsetsDirectional.only(
                              start: 1.0, end: 20.0),
                          color: kPrimaryDarkColorBrown,
                        ),
                      ),
                    ),
                    PhysicalModel(
                      borderRadius: new BorderRadius.circular(15.0),
                      color: Colors.transparent,
                      child: new Container(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: Text(
                          'OR',
                          style: TextStyle(
                              color: kPrimaryDarkColorBrown, fontSize: 11.0),
                          textAlign: TextAlign.center,
                        ),
                        width: 30.0,
                        height: 30.0,
                        decoration: new BoxDecoration(
                          borderRadius: new BorderRadius.circular(15.0),
                          border: new Border.all(
                            width: 2.0,
                            color: kPrimaryDarkColorBrown,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 1.0,
                      width: 120.0,
                      child: Center(
                        child: Container(
                          margin: new EdgeInsetsDirectional.only(
                              start: 20.0, end: 1.0),
                          color: kPrimaryDarkColorBrown,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 20.0),
                child: Text(
                  "Sign in with:",
                  style: TextStyle(color: kPrimaryDarkColorBrown),
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    child: FloatingActionButton(
                      onPressed: () {},
                      backgroundColor: Colors.red,
                      child: Text(
                        'g',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      child: FloatingActionButton(
                        onPressed: () {},
                        backgroundColor: Colors.blue,
                        child: Text(
                          'f',
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ],
              ),
            ],
          ),
        ),

        Container(
          padding: EdgeInsets.symmetric(vertical: 20.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                onPressed: moveToRegister,
                child: Text('Create an account',
                    style: TextStyle(
                        color: kPrimaryDarkColorBrown,
                        decoration: TextDecoration.underline)),
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                ),
//                    onPressed: validateAndSubmit
              ),
              FlatButton(
                  onPressed: () {},
                  child: Text(
                    'Forgot password',
                    style: TextStyle(
                        color: kPrimaryDarkColorBrown,
                        decoration: TextDecoration.underline),
                  ))
            ],
          ),
        ),
      ];
    } else {
      return [
        RaisedButton(
          child: Text('Create an account'),
          shape: BeveledRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(7.0)),
          ),
          elevation: 8.0,
          onPressed: () {
            validateAndSubmit();
          },
        ),
        FlatButton(
          onPressed: moveToLogin,
          child: Text(
            'Already have an account? Sign in.',
            style: TextStyle(color: kPrimaryDarkColorBrown),
          ),
          shape: BeveledRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(7.0)),
          ),
        ),
      ];
    }
  }
}
