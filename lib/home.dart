import 'dart:async';
import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

import 'auth.dart';
import 'colors.dart';

class Home extends StatefulWidget {
//  Home({this.auth});

  final BaseAuth auth;

//  final VoidCallback onSignedOut;
  const Home({
    Key key,
    @required this.auth,
//    @required this.onSignedOut,
  }) : super(key: key);

//        assert(frontTitle != null);
  @override
  State<StatefulWidget> createState() => new _HomeState();
}

class _HomeState extends State<Home> with AutomaticKeepAliveClientMixin<Home> {
//  String _path;
  File _profilePic;
  String _uid;
  String user;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    print("IN INIT STATE HOME");
    setState(() {
      retrieveUserId();
    });

    super.initState();
  }

  retrieveUserId() async {
    _uid = await widget.auth.currentUser().then((userId) {
      print("IN RETRIVE USER ID");
      return userId.uid.toString();
    });
    print("UID!!:  " + _uid.toString());

    try {
      await downloadFile(); //and download profile pic
    } catch (e) {
      return SnackBar(
        content: Text('No internet connection. Unable to load data.'),
        duration: Duration(seconds: 5),
      );
    }
  }

//
  Future<Null> downloadFile() async {
//    final RegExp regExp = RegExp('([^?/]*\.(jpg))');
//    final String fileName = regExp.stringMatch(httpPath);

//    print("IN DOWNLOAD FILE 1");
    final String fileName = "profile_pic.jpg";
    final Directory tempDir = Directory.systemTemp;
    final File file = File('${tempDir.path}/$fileName');
//    print("IN DOWNLOAD FILE 2");
//    print("UID: $_uid");
//    print("FILENAME: $fileName");

    final StorageReference ref = FirebaseStorage.instance
        .ref()
        .child("users")
        .child(_uid)
        .child(fileName);
//    print("IN DOWNLOAD FILE 3");
    final StorageFileDownloadTask downloadTask = ref.writeToFile(file);
    final int byteNumber = (await downloadTask.future).totalByteCount;
    print(byteNumber);
//    print("IN DOWNLOAD FILE 4");

//    print("PATH FOR DOWNLOAD: IN DOWNLOAD");
    setState(() {
      _profilePic = file;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kPaperWhite,
        body: Container(
          color: Colors.black54,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  color: Colors.black54,
                  width: 80.0,
                  height: 80.0,
                  child: _profilePic != null
                      ? Image.asset(_profilePic.path)
                      : Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50.0)),
                        )),
            ],
          ),
        )

//      StreamBuilder(
//        stream: Firestore.instance.collection('books@exchange').snapshots(),
//        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
//          if (!snapshot.hasData) return CircularProgressIndicator();
//          return FirestoreListView(documents: snapshot.data.documents);
//        },
        );
  }
}

//class FirestoreListView extends StatelessWidget {
//  final List<DocumentSnapshot> documents;
//
//  FirestoreListView({this.documents});
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//
//    return ListView.builder(
//        itemCount: documents.length,
//        itemExtent: 170.0,
//        itemBuilder: (BuildContext context, int index) {
//          String username = documents[index].data['title'].toString();
////          String author = documents[index].data['author'].toString();
////          String isbn = documents[index].data['isbn'].toString();
//
//          return ListTile(
//            contentPadding:
//                EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
//            title: Container(
//              margin: EdgeInsets.symmetric(horizontal: 10.0),
//              decoration: BoxDecoration(
//                  gradient: LinearGradient(
//                      begin: Alignment.bottomRight,
//                      end: new Alignment(0.1, 0.0),
//                      colors: [
//                        kPrimaryLightColorBrown,
//                        kPrimaryDarkColorBrown,
//                      ]),
//                  borderRadius: BorderRadius.circular(5.0),
////                    border: Border.all(color: kPaleWhite),
////                    color: kPrimaryDarkColorBrown,
//                  boxShadow: [
//                    BoxShadow(
//                        blurRadius: 1.0,
//                        offset: Offset(4.0, 4.0),
//                        color: Colors.black38)
//                  ]),
//              child: Text(
//                username,
//                softWrap: true,
//                maxLines: 3,
//                overflow: TextOverflow.ellipsis,
//                style: TextStyle(
//                  fontWeight: FontWeight.w700,
//                ),
//              ),
//            ),
//          );
//        });
//  }
//}
