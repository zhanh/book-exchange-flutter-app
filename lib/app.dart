import 'package:flutter/material.dart';

import 'auth.dart';
import 'colors.dart';
import 'root_page.dart';
import 'startup_page.dart';
import 'supplemental/cut_corners_border.dart';
//import 'my_library.dart';

final ThemeData _kAppTheme = _buildAppTheme();

class BookExchangeApp extends StatefulWidget {
  BookExchangeApp({Key key}) : super(key: key);

  @override
  _BookExchangeAppState createState() {
    return new _BookExchangeAppState();
  }
}

class _BookExchangeAppState extends State<BookExchangeApp> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Book Exchange',
      initialRoute: '/',
      routes: {
        '/': (context) => StartupPage(key: widget.key),
        "/root_page": (context) => RootPage(auth: Auth()),

//        "/discover": (_) => Backdrop(
//              frontTitle: Menu(),
//              frontLayer: Discover(),
//              backTitle: Text("MENU"),
//              backLayer: Text("DISCOVER"),
//            ),
//        "/login_page": (_) => LoginPage(),
//        "/my_library": (_) => BackdropMenu(
//              frontLayer: MyLibrary(),
//              frontTitle: Text("MY LIBRARY"),
//            ),
//        "/home": (_) =>
//            BackdropMenu(frontLayer: Home(), frontTitle: Text("HOME")),
      },
//      home: Backdrop(
//          frontLayer: Home(auth: ),
//          backLayer: Menu(),
//          frontTitle: Text("Discover"),
//          backTitle: Text("MENU")),

//
//      Backdrop(
//          frontLayer: Profile(),
//          backLayer: Menu(),
//          frontTitle: Text("PROFILE"),
//          backTitle: Text("MENU")),

      theme: _kAppTheme,
    );
  }
}

TextTheme _buildAppTextTheme(TextTheme base) {
  return base
      .copyWith(
        headline: base.headline.copyWith(
            fontWeight: FontWeight.w500,
            fontSize: 18.0,
            color: kPrimaryTextColor),
        title: base.title.copyWith(color: kPrimaryTextColor, fontSize: 18.0),
      )
      .apply(fontFamily: 'Poppins');
}

//dark theme
ThemeData _buildAppTheme() {
  final ThemeData base = ThemeData.dark();
  return base.copyWith(
    accentColor: kDarkGrey,
    primaryColor: kSecondaryDarkColorBrown,
    buttonColor: kSecondaryDarkColorBrown,
    scaffoldBackgroundColor: kPrimaryColorAmber,
    cardColor: kSecondaryDarkColorBrown,
//    textSelectionColor: kPrimaryTextColor,
    errorColor: kErrorRed,
    textTheme: _buildAppTextTheme(base.textTheme),
    primaryTextTheme: _buildAppTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildAppTextTheme(base.accentTextTheme),
    primaryIconTheme: base.iconTheme.copyWith(color: kSecondaryDarkColorBrown),
    hintColor: kPrimaryDarkColorBrown,
    inputDecorationTheme: InputDecorationTheme(
      border: CutCornersBorder(),
    ),
  );
}

//light theme
//ThemeData _buildAppTheme() {
//  final ThemeData base = ThemeData.light();
//  return base.copyWith(
//    accentColor: kShrineBrown900,
//    primaryColor: kShrinePink100,
//    buttonColor: kShrinePink100,
//    scaffoldBackgroundColor: kShrineBackgroundWhite,
//    cardColor: kShrineBackgroundWhite,
//    textSelectionColor: kShrinePink100,
//    errorColor: kShrineErrorRed,
//
//    //TODO: Add the text themes (103)
//    textTheme: _buildAppTextTheme(base.textTheme),
//    primaryTextTheme: _buildAppTextTheme(base.primaryTextTheme),
//    accentTextTheme: _buildAppTextTheme(base.accentTextTheme),
//
//    //TODO: Add the icon themes (103)
//    primaryIconTheme: base.iconTheme.copyWith(
//      color: kShrineBrown900,
//    ),
//    //TODO: Decorate the inputs (103)
//    inputDecorationTheme: InputDecorationTheme(
////        border: OutlineInputBorder()
//      border: CutCornersBorder(),
//    ),
//  );
//}
