import 'package:flutter/material.dart';
//
//const kShrinePink50 = const Color(0xFFFEEAE6);
//const kShrinePink100 = const Color(0xFFFEDBD0);
//const kShrinePink300 = const Color(0xFFFBB8AC);
//
//const kShrineBrown900 = const Color(0xFF442B2D);
//
//const kShrineErrorRed = const Color(0xFFC5032B);
//
//const kShrineSurfaceWhite = const Color(0xFFFFFBFA);
//const kShrineBackgroundWhite = Colors.white;
//
//const kShrineAltDarkGrey = const Color(0xFF414149);
//const kShrineAltYellow = const Color(0xFFFFCF44);

const kDarkNavy = const Color(0xFF262B3E);
const kDarkGrey = const Color(0xFF414149);
const kPaleWhite = const Color(0xFFE4E4E4);
const kErrorRed = const Color(0xFFC5032B);

const kPrimaryColorAmber = const Color(0xFFFFC928);
const kprimaryLightColorAmber = const Color(0xFFfffc61);

const kPrimaryLightColorBrown = const Color(0xFFc79900);
const kSecondaryLightColorBrown = const Color(0xFFa98274);
const kPrimaryDarkColorBrown = const Color(0xFF795548);
const kSecondaryDarkColorBrown = const Color(0xFF4b2c20);

const kPrimaryTextColor = const Color(0xFF4b2c20);
const kSecondaryTextColor = const Color(0xFF4b2c20);
const kPaperWhite = const Color(0xFFFFFBFA);
