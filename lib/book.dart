//import 'package:flutter/material.dart';
//
//class Book {
//  String id;
//  String title;
//  String author;
//  String isbn;
//  String image;
//  String owner;
//
//  Book(
//      this.id, this.title, this.author, this.isbn, this.image, this.owner);
//}

//import 'package:flutter/material.dart';
//
//import 'colors.dart';
////import 'package:cloud_firestore/cloud_firestore.dart';
//
//class MyLibrary extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return new Scaffold(
//      backgroundColor: kPaperWhite,
//      body: MyBookList(),
//    );
//  }
//}
//
//class MyBookList extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    var _bookTitle = "Dawn of Wonder";
//    var _author = "Jonathan Renshaw";
//
//    Widget bookCard = Container(
//      child: Row(
//        crossAxisAlignment: CrossAxisAlignment.center,
//        children: <Widget>[
//          Container(
//            color: kSecondaryLightColorBrown,
//            width: 128.0,
//            height: 128.0,
//          ),
//          Column(
//            mainAxisAlignment: MainAxisAlignment.center,
//            crossAxisAlignment: CrossAxisAlignment.start,
//            children: <Widget>[
//              Container(
//                  child: Text(_bookTitle),
//                  padding: EdgeInsets.fromLTRB(16.0, 4.0, 16.0, 4.0)),
//              SizedBox(height: 8.0),
//              Container(
//                  child: Text(_author),
//                  padding: EdgeInsets.fromLTRB(16.0, 4.0, 16.0, 4.0))
//            ],
//          ),
//        ],
//      ),
//    );
//
//    var card = Card(
//      margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
//      elevation: 0.0,
//      child: bookCard,
//    );
//
//    return ListView(
//      children: <Widget>[
//        card,
//        card,
//        card,
//        card,
//        card,
//        card,
//        card,
//        card,
//        card,
//      ],
//    );
//  }
//}

//List<Card> _buildListCards(BuildContext context) {
//  var _bookTitle = "Dawn of Wonder";
//  var _author = "Jonathan Renshaw";
//
//  Widget titleSection = Container(
//    padding: const EdgeInsets.all(32.0),
//    child: Row(
//      children: [
//        Expanded(
//          child: Column(
//            crossAxisAlignment: CrossAxisAlignment.start,
//            children: [
//              Container(
//                padding: const EdgeInsets.only(bottom: 8.0),
//                child: Text(
//                  _bookTitle,
//                  style: TextStyle(
//                    fontWeight: FontWeight.bold,
//                  ),
//                ),
//              ),
//              Text(
//                _author,
//                style: TextStyle(
//                  color: Colors.grey[500],
//                ),
//              ),
//            ],
//          ),
//        ),
//        Icon(
//          Icons.star,
//          color: Colors.red[500],
//        ),
//        Text('41'),
//      ],
//    ),
//  );
//
//  List<Book> books = getBooks();
//
//  if (books == null || books.isEmpty) {
//    return const <Card>[];
//  }
//
//  return books.map((book) {
//    return Card(
//      elevation: 4.0,
//      child: ListView(
//        padding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 0.0),
//        children: <Widget>[
//          ListTile(
//            leading: IconButton(
//                icon: Icon(Icons.image),
//                onPressed: () {
//                  print("Image button pressed");
//                }),
//            title: titleSection,
//          )
//        ],
//      ),
//    );
//  }).toList();
//}
//class MyHomePage extends StatelessWidget {
//  const MyHomePage({Key key, this.title}) : super(key: key);
//
//  final String title;
//
//  @override
//  Widget build(BuildContext context) {
//    return new Scaffold(
//      appBar: new AppBar(title: new Text(title)),
//      body: new StreamBuilder(
//          stream: Firestore.instance.collection('books@exchange').snapshots(),
//          builder: (context, snapshot) {
//            if (!snapshot.hasData) return const Text('Loading...');
//            return new ListView.builder(
//                itemCount: snapshot.data.documents.length,
//                padding: const EdgeInsets.only(top: 10.0),
//                itemExtent: 25.0,
//                itemBuilder: (context, index) {
//                  DocumentSnapshot ds = snapshot.data.documents[index];
//                  return new Text(
//                      " ${ds['title']} ${ds['author']} ${ds['isbn']} ${ds['year_pub']}");
//                });
//          }),
//    );
//  }
//}
