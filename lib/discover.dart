import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import './model/book.dart';
import 'auth.dart';
import 'colors.dart';

class Discover extends StatefulWidget {
  final BaseAuth auth;

  Discover({
    @required this.auth,
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _DiscoverState();
}

class _DiscoverState extends State<Discover> {
  String _uid;

  void initState() {
    // TODO: implement initState

//    print(widget.auth.currentUser().);
    widget.auth.currentUser().then((userId) {
      setState(() {
        userId == null
            ? print("ERROR IN INIT STATE LIBRARY USERID NULL")
            : _uid = userId.uid.toString();
        print('USERID IN INIT STATE: $_uid');
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var collectionReference = Firestore.instance.collection('books@exchange');
    var query = collectionReference.where('public', isEqualTo: true);
    return Scaffold(
        backgroundColor: kPaperWhite,
        body: ListView.builder(
            itemCount: 4,
            itemBuilder: (context, index) {
              switch (index) {
                case 0:
                  Row(children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 12.0),
                      child: Icon(
                        Icons.stars,
                        color: kSecondaryDarkColorBrown,
                      ),
                    ),
                    Text(
                      'FEATURED',
                      style: TextStyle(color: kSecondaryDarkColorBrown),
                    ),
                  ]);
                  break;
                case 1:
                  Expanded(
                      child: StreamBuilder(
                    stream: query.snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) return CircularProgressIndicator();
                      return FirestoreListView(
                          documents: snapshot.data.documents);
                    },
                  ));
              }
            }));
  }
}

class FirestoreListView extends StatelessWidget {
  final List<DocumentSnapshot> documents;

  FirestoreListView({this.documents});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print("IN BUILD");

    return GridView.builder(
        itemCount: documents.length,
        scrollDirection: Axis.horizontal,
        gridDelegate:
            new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1),
        itemBuilder: (BuildContext context, int index) {
          String title = documents[index].data['title'].toString();
          String author = documents[index].data['author'].toString();
          String isbn13 = documents[index].data['isbn13'].toString();
//          List<Object> books = documents[index].data['library'];
//          print('isbn: ' + isbn);
//          List<dynamic> library = documents[index].data['library'];
          print("DOC LENGTH: ${documents.length}");
          print("title $title");
          print("author $author");
          print("isbn13 $isbn13");
//          print(title + ' ' + author);
//          Future<Book> fetchPost() async {
//            var uri = 'https://www.googleapis.com/books/v1/volumes?q=' + isbn;
//            print('uri: ' + uri);
//            final response = await http.get(uri);
//            if (response.statusCode == 200) {
//              return Book.fromJson(json.decode(response.body));
//            } else {
//              throw Exception('Error: Failed to load');
//            }
//          }
          Future<Book> fetchPost() async {
            var uri = 'https://www.googleapis.com/books/v1/volumes?q=' + isbn13;
//            print("FETCH POST USER: " + widget.auth.uid());
            print("fetching response");
            print('uri: ' + uri);
            final response = await http.get(uri);
            if (response.statusCode == 200) {
              return Book.fromJson(json.decode(response.body));
            } else {
              throw Exception('Error: Failed to load');
            }
          }

          return ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              title: Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.bottomRight,
                        end: new Alignment(0.1, 0.0),
                        colors: [
                          kPrimaryDarkColorBrown,
                          kSecondaryDarkColorBrown,
                        ]),
                    borderRadius: BorderRadius.circular(5.0),
//                    border: Border.all(color: kPaleWhite),
//                    color: kPrimaryDarkColorBrown,
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 1.0,
                          offset: Offset(4.0, 4.0),
                          color: Colors.black38)
                    ]),
                child: Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
                      width: 108.0,
//                      height: 160.0,
//                      padding: EdgeInsets.all(20.0),
                      color: kPrimaryDarkColorBrown,
                      child: FutureBuilder<Book>(
                          future: fetchPost(),
                          builder: (BuildContext context,
                              AsyncSnapshot<Book> snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.none:
                                return SnackBar(
                                  content: Row(
                                    children: <Widget>[
                                      Text(
                                          'No internet connection. Unable to load data.'),
                                      FlatButton(
                                          onPressed: () {
                                            //TODO REFRESH PAGE FUNCTION
                                          },
                                          child: Text('TAP TO RETRY'))
                                    ],
                                  ),
                                  duration: Duration(seconds: 5),
                                );
                              case ConnectionState.waiting:
                                return Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 40.0, vertical: 55.0),
                                    child: CircularProgressIndicator(
                                      valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              kPrimaryColorAmber),
                                    ));
                                break;
                              default:
                                if (snapshot.hasError) {
                                  print('Error: in error snapshot' +
                                      snapshot.error);
                                  return new Container(
                                    width: 140.0,
                                    height: 160.0,
                                    color: kprimaryLightColorAmber,
                                    child: Icon(
                                      Icons.broken_image,
                                      color: kPrimaryDarkColorBrown,
                                    ),
                                  );
                                } else {
                                  var smallThumbnailUrl =
                                      snapshot.data.smallThumbnailUrl;
                                  print('image url: ' +
                                      smallThumbnailUrl.toString());
                                  return Container(
                                      decoration: BoxDecoration(
                                          border:
                                              Border.all(color: kPaperWhite)),
                                      child: Image.network(
                                        smallThumbnailUrl,
                                        fit: BoxFit.cover,
                                      ));
                                }
                            }
                          }),
                    ),
                    Expanded(
                        child: Container(
                      margin: EdgeInsets.fromLTRB(0.0, 0.0, 20.0, 12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            title,
                            softWrap: true,
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(height: 4.0),
                          Text(author,
                              softWrap: true,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontStyle: FontStyle.italic)),
                        ],
                      ),
                    )),
                  ],
                ),
              ));
        });
  }
}
////  var uri = 'https://www.googleapis.com/books/v1/volumes?q=';
//
////  List<Book> parseBooks(String responseBody) {
////    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
////
////    return parsed.map<Book>((json) => Book.fromJson(json)).toList();
////  }
////
////  Future<List<Book>> fetchBooks(http.Client client, String isbn) async {
////    final response = await client.get(uri + isbn);
////
////    return compute(parseBooks, response.body);
////  }
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//
//    return Scaffold(
//      backgroundColor: kPaperWhite,
//      body: StreamBuilder(
//        stream: Firestore.instance.collection('users/').snapshots(),
////        builder: (context, snapshot) {
////          if (!snapshot.hasData) return const Text("LOADING");
////          return ListView.builder(
////              itemCount: snapshot.data.documents.length,
////              padding: EdgeInsets.only(top: 10.0),
////              itemExtent: 25.0,
////              itemBuilder: (context, index) {
////                DocumentSnapshot ds = snapshot.data.documents[index];
////                return new Text(
////                  "${ds['title']}, ${ds['author']}",
////                  style: TextStyle(color: Colors.black12),
////                );
////              });
////        },
//        builder: (context, snapshot) {
//          if (!snapshot.hasData) {
//            return CircularProgressIndicator();
//          } else {
//            return FsListView(documents: snapshot.data.documents);
////            return new ListView.builder(
////                itemCount: snapshot.data.documents.length,
////                itemExtent: 170.0,
////                itemBuilder: (context, index) {
////                  DocumentSnapshot ds = snapshot.data.documents[index];
////                  String title = ds['title'].toString();
////                  String author = ds['author'].toString();
////                  String isbn = ds['isbn'].toString();
////
////                  print('isbn: ' + isbn);
////
////                  return ListTile(
////                      contentPadding:
////                          EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
////                      title: Container(
////                        margin: EdgeInsets.symmetric(horizontal: 10.0),
////                        decoration: BoxDecoration(
////                            gradient: LinearGradient(
////                                begin: Alignment.bottomRight,
////                                end: new Alignment(0.1, 0.0),
////                                colors: [
////                                  kPrimaryLightColorBrown,
////                                  kPrimaryDarkColorBrown,
////                                ]),
////                            borderRadius: BorderRadius.circular(5.0),
//////                    border: Border.all(color: kPaleWhite),
//////                    color: kPrimaryDarkColorBrown,
////                            boxShadow: [
////                              BoxShadow(
////                                  blurRadius: 1.0,
////                                  offset: Offset(4.0, 4.0),
////                                  color: Colors.black38)
////                            ]),
////                        child: Row(
////                          children: <Widget>[
////                            Container(
////                              child: (FutureBuilder<List<Book>>(
////                                future: fetchPost(http.Client(), isbn),
////                                builder: (context, snapshot) {
////                                  if (snapshot.hasError) {
////                                    print('Error: ' + snapshot.error);
////                                    return new Container(
////                                      width: 140.0,
////                                      height: 160.0,
////                                      color: kprimaryLightColorAmber,
////                                      child: Icon(
////                                        Icons.broken_image,
////                                        color: kPrimaryDarkColorBrown,
////                                      ),
////                                    );
////                                  }
//////                                  if (snapshot.hasData) {
//////                                    print("BEFORE SMALL THUMBNAIL URL");
//////                                    var smallThumbnailUrl =
//////                                        snapshot.data.smallThumbnailUrl;
//////                                    print('image url: ' +
//////                                        smallThumbnailUrl.toString());
//////                                    print("AFTER SMALL THUMBNAIL URL");
//////                                    return Container(
//////                                        decoration: BoxDecoration(
//////                                            border:
//////                                                Border.all(color: kPaperWhite)),
//////                                        child: Image.network(
//////                                          smallThumbnailUrl,
//////                                          fit: BoxFit.cover,
//////                                        ));
//////                                  } else if (snapshot.hasError) {
////
//////                                  return CircularProgressIndicator();
////                                  return snapshot.hasData
////                                      ? BooksList(books: snapshot.data)
////                                      : Center(
////                                          child: CircularProgressIndicator());
////                                },
////                              )),
////                              margin:
////                                  EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
////                              width: 108.0,
////                              color: kSecondaryDarkColorBrown,
////                            ),
////                            Expanded(
////                                child: Container(
////                              margin: EdgeInsets.fromLTRB(0.0, 0.0, 20.0, 0.0),
////                              child: Column(
////                                crossAxisAlignment: CrossAxisAlignment.start,
////                                mainAxisAlignment: MainAxisAlignment.center,
////                                children: <Widget>[
////                                  Text(
////                                    title,
////                                    softWrap: true,
////                                    maxLines: 3,
////                                    overflow: TextOverflow.ellipsis,
////                                    style: TextStyle(
////                                      fontWeight: FontWeight.w700,
////                                    ),
////                                  ),
////                                  SizedBox(height: 20.0),
////                                  Text(author,
////                                      softWrap: true,
////                                      maxLines: 2,
////                                      overflow: TextOverflow.ellipsis,
////                                      style: TextStyle(
////                                          fontStyle: FontStyle.italic)),
////                                ],
////                              ),
////                            )),
////                          ],
////                        ),
////                      ));
////                });
//          }
//        },
//      ),
//    );
//  }
//}
//
//class FsListView extends StatelessWidget {
//  final List<DocumentSnapshot> documents;
//
//  FsListView({this.documents});
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return ListView.builder(
//        itemCount: documents.length,
//        itemExtent: 170.0,
//        itemBuilder: (BuildContext context, int index) {
//          String title = documents[index].data['title'].toString();
//          String author = documents[index].data['author'].toString();
//          String isbn = documents[index].data['isbn'].toString();
//
//          print('isbn: ' + isbn);
//
//          Future<Book> fetchPost() async {
//            var uri = 'https://www.googleapis.com/books/v1/volumes?q=' + isbn;
////            print("FETCH POST USER: " + widget.auth.uid());
//            print("fetching response");
//            print('uri: ' + uri);
//            final response = await http.get(uri);
//            if (response.statusCode == 200) {
//              return Book.fromJson(json.decode(response.body));
//            } else {
//              throw Exception('Error: Failed to load');
//            }
//          }
//
//          return ListTile(
//              contentPadding:
//                  EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
//              title: Container(
//                margin: EdgeInsets.symmetric(horizontal: 10.0),
//                decoration: BoxDecoration(
//                    gradient: LinearGradient(
//                        begin: Alignment.bottomRight,
//                        end: new Alignment(0.1, 0.0),
//                        colors: [
//                          kPrimaryLightColorBrown,
//                          kPrimaryDarkColorBrown,
//                        ]),
//                    borderRadius: BorderRadius.circular(5.0),
////                    border: Border.all(color: kPaleWhite),
////                    color: kPrimaryDarkColorBrown,
//                    boxShadow: [
//                      BoxShadow(
//                          blurRadius: 1.0,
//                          offset: Offset(4.0, 4.0),
//                          color: Colors.black38)
//                    ]),
//                child: Row(
//                  children: <Widget>[
//                    Container(
//                      child: FutureBuilder<Book>(
//                        future: fetchPost(),
//                        builder: (BuildContext context,
//                            AsyncSnapshot<Book> snapshot) {
////                          print("IN SNAPSHOT: " + snapshot.toString());
//
//                          try {
//                            if (snapshot.hasData) {
//                              var smallThumbnailUrl =
//                                  snapshot.data.smallThumbnailUrl;
//                              print(
//                                  'image url: ' + smallThumbnailUrl.toString());
//                              return Container(
//                                  decoration: BoxDecoration(
//                                      border: Border.all(color: kPaperWhite)),
//                                  child: Image.network(
//                                    smallThumbnailUrl,
//                                    fit: BoxFit.cover,
//                                  ));
//                              //                            print('hasData; title: ' + snapshot.data.title);
//                            } else if (snapshot.hasError) {
//                              print(
//                                  'Error: in error snapshot' + snapshot.error);
//                              return new Container(
//                                width: 140.0,
//                                height: 160.0,
//                                color: kprimaryLightColorAmber,
//                                child: Icon(
//                                  Icons.broken_image,
//                                  color: kPrimaryDarkColorBrown,
//                                ),
//                              );
//                            }
//                          } catch (e) {
//                            print("TEST ERROR CATCH");
//                            switch (snapshot.connectionState) {
//                              case ConnectionState.none:
//                                return SnackBar(
//                                  content: Text(
//                                      'No internet connection. Unable to load data.'),
//                                  duration: Duration(seconds: 5),
//                                );
//                              case ConnectionState.waiting:
//                                return CircularProgressIndicator();
//                            }
//                          }
//
//                          return new Container(
//                            width: 140.0,
//                            height: 160.0,
//                            color: kSecondaryDarkColorBrown,
//                            child: Icon(
//                              Icons.broken_image,
//                              color: kPrimaryDarkColorBrown,
//                            ),
//                          );
//                        },
//                      ),
//                      margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
//                      width: 108.0,
//                      color: kSecondaryDarkColorBrown,
//                    ),
//                    Expanded(
//                        child: Container(
//                      margin: EdgeInsets.fromLTRB(0.0, 0.0, 20.0, 0.0),
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        children: <Widget>[
//                          Text(
//                            title,
//                            softWrap: true,
//                            maxLines: 3,
//                            overflow: TextOverflow.ellipsis,
//                            style: TextStyle(
//                              fontWeight: FontWeight.w700,
//                            ),
//                          ),
//                          SizedBox(height: 20.0),
//                          Text(author,
//                              softWrap: true,
//                              maxLines: 2,
//                              overflow: TextOverflow.ellipsis,
//                              style: TextStyle(fontStyle: FontStyle.italic)),
//                        ],
//                      ),
//                    )),
//                  ],
//                ),
//              ));
//        });
//  }
//}

//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return new Scaffold(
//      backgroundColor: kPaperWhite,
//      body: _DiscoverState(),
//    );
//  }

//class _DiscoverState extends State<Discover> {
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//
//    var _bookTitle = "Dawn of Wonder";
//    var _author = "Jonathan Renshaw";
//
//    Widget bookCard = Container(
//      child: Row(
//        children: <Widget>[
//          Container(
//            color: kSecondaryLightColorBrown,
//            width: 128.0,
//            height: 128.0,
//            margin: EdgeInsets.symmetric(horizontal: 8.0),
//          ),
//        ],
//      ),
//    );
//
//    return new ListView(
//      scrollDirection: Axis.horizontal,
//      children: <Widget>[bookCard, bookCard, bookCard, bookCard],
//    );

//class Discover extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return new Scaffold(
//      backgroundColor: kPaperWhite,
//      body: _DiscoverState(),
//    );
//  }
//}

//class _DiscoverState extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//
//    var _bookTitle = "Dawn of Wonder";
//    var _author = "Jonathan Renshaw";
//
//    Widget bookCard = Container(
//      child: Row(
//        children: <Widget>[
//          Container(
//            color: kSecondaryLightColorBrown,
//            width: 128.0,
//            height: 128.0,
//            margin: EdgeInsets.symmetric(horizontal: 8.0),
//          ),
//        ],
//      ),
//    );
//
//    return new ListView(
//      scrollDirection: Axis.horizontal,
//      children: <Widget>[bookCard, bookCard, bookCard, bookCard],
//    );

//            scrollDirection: Axis.horizontal,
//            children: <Widget>[
//              Card(
//                margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
//                elevation: 0.0,
//                child: bookCard,
//              ),
//              Card(
//                margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
//                elevation: 0.0,
//                child: bookCard,
//              ),
//              Card(
//                margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
//                elevation: 0.0,
//                child: bookCard,
//              ),
//              Card(
//                margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
//                elevation: 0.0,
//                child: bookCard,
//              ),
//            ],
//          ),
//        ],
//      ),
//    );
//  }
//}
//
//class BooksList extends StatelessWidget {
//  final List<Book> books;
//
//  BooksList({Key key, this.books}) : super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return ListView.builder(
//        itemExtent: 160.0,
//        itemCount: books.length,
//        itemBuilder: (context, index) {
//          return Container(
//              decoration: BoxDecoration(border: Border.all(color: kPaperWhite)),
//              child: Image.network(
//                books[index].smallThumbnailUrl,
//                fit: BoxFit.cover,
//              ));
//        });
//  }
//}
