//import 'dart:async';
//import 'dart:io';
//
//import 'package:flutter/material.dart';
//import 'package:image_picker/image_picker.dart';
//import 'package:mlkit/mlkit.dart';
//
//import 'colors.dart';
//
//class BarcodeScan extends StatefulWidget {
//  @override
//  BarcodeScanState createState() {
//    return new BarcodeScanState();
//  }
//}
//
//class BarcodeScanState extends State<BarcodeScan> {
//  static const String CAMERA_SOURCE = 'CAMERA_SOURCE';
//  static const String GALLERY_SOURCE = 'GALLERY_SOURCE';
//  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//  File _file;
//
//  Widget buildImageRow(BuildContext context, File file) {
//    return SizedBox(
//        height: 500.0,
//        child: Image.file(
//          file,
//          fit: BoxFit.fitWidth,
//        ));
//  }
//
//  Widget buildDeleteRow(BuildContext context) {
//    return Center(
//      child: Padding(
//        padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
//        child: RaisedButton(
//            color: Colors.red,
//            textColor: Colors.white,
//            splashColor: Colors.blueGrey,
//            onPressed: () {
//              setState(() {
//                _file = null;
//              });
//            },
//            child: const Text('Delete Image')),
//      ),
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    void onPickImageSelected(String source) async {
//      var imageSource;
//      if (source == CAMERA_SOURCE) {
//        imageSource = ImageSource.camera;
//      } else {
//        imageSource = ImageSource.gallery;
//      }
//
//      final scaffold = _scaffoldKey.currentState;
//
//      try {
//        final file = await ImagePicker.pickImage(source: imageSource);
//        if (file == null) {
//          throw Exception('File is not available');
//        }
//
//        Navigator.push(
//          context,
//          new MaterialPageRoute(builder: (context) => DetailWidget(file)),
//        );
//      } catch (e) {
//        scaffold.showSnackBar(SnackBar(
//          content: Text(e.toString()),
//        ));
//      }
//    }
//
//    Widget buildSelectImageRowWidget(BuildContext context) {
//      return Row(
//        children: <Widget>[
//          Expanded(
//              child: Padding(
//            padding: EdgeInsets.symmetric(horizontal: 8.0),
//            child: RaisedButton(
//                color: Colors.blue,
//                textColor: Colors.white,
//                splashColor: Colors.blueGrey,
//                onPressed: () {
//                  onPickImageSelected(CAMERA_SOURCE);
//                },
//                child: const Text('Camera')),
//          )),
//          Expanded(
//              child: Padding(
//            padding: EdgeInsets.symmetric(horizontal: 8.0),
//            child: RaisedButton(
//                color: Colors.blue,
//                textColor: Colors.white,
//                splashColor: Colors.blueGrey,
//                onPressed: () {
//                  onPickImageSelected(GALLERY_SOURCE);
//                },
//                child: const Text('Gallery')),
//          ))
//        ],
//      );
//    }
//
//    return Scaffold(
//        key: _scaffoldKey,
//        backgroundColor: kDarkGrey,
//        appBar: AppBar(
//          leading: IconButton(
//              icon: Icon(
//                Icons.clear,
//                color: kPaperWhite,
//              ),
//              onPressed: () {
//                Navigator.pop(context);
//              }),
//          centerTitle: true,
//          title: Text('SCAN BARCODE', style: TextStyle(color: kPaperWhite)),
//        ),
//        body: Center(
//          child: Column(
//            crossAxisAlignment: CrossAxisAlignment.center,
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//              Row(
//                mainAxisAlignment: MainAxisAlignment.center,
//                children: <Widget>[
//                  GestureDetector(
//                    child: Container(
//                      width: 160.0,
//                      height: 160.0,
//                      color: Colors.blue,
//                    ),
//                    onTap: () {
//                      onPickImageSelected(CAMERA_SOURCE);
//                    },
//                  ),
//                ],
//              ),
//              SizedBox(
//                height: 20.0,
//              ),
//
////          buildSelectImageRowWidget(context),
//            ],
//          ),
//        ));
//  }
//}
//
//class DetailWidget extends StatefulWidget {
//  File _file;
//
//  DetailWidget(this._file);
//
//  @override
//  State<StatefulWidget> createState() {
//    return _DetailState();
//  }
//}
//
//class _DetailState extends State<DetailWidget> {
//  FirebaseVisionBarcodeDetector barcodeDetector =
//      FirebaseVisionBarcodeDetector.instance;
//  List<VisionBarcode> _currentBarcodeLabels = <VisionBarcode>[];
//
//  @override
//  void initState() {
//    super.initState();
//
//    Timer(Duration(milliseconds: 1000), () {
//      this.analyzeLabels();
//    });
//  }
//
//  void analyzeLabels() async {
//    try {
//      var currentLabels;
//
//      currentLabels = await barcodeDetector.detectFromPath(widget._file.path);
//      setState(() {
//        _currentBarcodeLabels = currentLabels;
//      });
//    } catch (e) {
//      print(e.toString());
//    }
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//        appBar: AppBar(
//          centerTitle: true,
//          title: Text('Barcode Scanner'),
//        ),
//        body: Column(
//          children: <Widget>[
//            buildImage(context),
//            buildBarcodeList(_currentBarcodeLabels)
//          ],
//        ));
//  }
//
//  Widget buildImage(BuildContext context) {
//    return Expanded(
//      flex: 2,
//      child: Container(
//          decoration: BoxDecoration(color: Colors.black),
//          child: Center(
//            child: widget._file == null
//                ? Text('No Image')
//                : FutureBuilder<Size>(
//                    future: _getImageSize(
//                        Image.file(widget._file, fit: BoxFit.fitWidth)),
//                    builder:
//                        (BuildContext context, AsyncSnapshot<Size> snapshot) {
//                      if (snapshot.hasData) {
//                        return Container(
//                            foregroundDecoration: BarcodeDetectDecoration(
//                                _currentBarcodeLabels, snapshot.data),
//                            child:
//                                Image.file(widget._file, fit: BoxFit.fitWidth));
//                      } else {
//                        return CircularProgressIndicator();
//                      }
//                    },
//                  ),
//          )),
//    );
//  }
//
//  Widget buildBarcodeList(List<VisionBarcode> barcodes) {
//    if (barcodes.length == 0) {
//      return Expanded(
//        flex: 1,
//        child: Center(
//          child: Text('No barcode detected',
//              style: Theme.of(context).textTheme.subhead),
//        ),
//      );
//    }
//    return Expanded(
//      flex: 1,
//      child: Container(
//        child: ListView.builder(
//            padding: const EdgeInsets.all(1.0),
//            itemCount: barcodes.length,
//            itemBuilder: (context, i) {
//              final barcode = barcodes[i];
//              var text = "Raw Value: ${barcode.rawValue}";
//              return _buildTextRow(text);
//            }),
//      ),
//    );
//  }
//
//  Widget buildTextList(List<VisionText> texts) {
//    if (texts.length == 0) {
//      return Expanded(
//          flex: 1,
//          child: Center(
//            child: Text('No text detected',
//                style: Theme.of(context).textTheme.subhead),
//          ));
//    }
//    return Expanded(
//      flex: 1,
//      child: Container(
//        child: ListView.builder(
//            padding: const EdgeInsets.all(1.0),
//            itemCount: texts.length,
//            itemBuilder: (context, i) {
//              return _buildTextRow(texts[i].text);
//            }),
//      ),
//    );
//  }
//
//  Widget _buildTextRow(text) {
//    return ListTile(
//      title: Text(
//        "$text",
//      ),
//      dense: true,
//    );
//  }
//
//  Future<Size> _getImageSize(Image image) {
//    Completer<Size> completer = Completer<Size>();
//    image.image.resolve(ImageConfiguration()).addListener(
//        (ImageInfo info, bool _) => completer.complete(
//            Size(info.image.width.toDouble(), info.image.height.toDouble())));
//    return completer.future;
//  }
//}
//
///*
//  This code uses the example from azihsoyn/flutter_mlkit
//  https://github.com/azihsoyn/flutter_mlkit/blob/master/example/lib/main.dart
//*/
//
//class BarcodeDetectDecoration extends Decoration {
//  final Size _originalImageSize;
//  final List<VisionBarcode> _barcodes;
//
//  BarcodeDetectDecoration(List<VisionBarcode> barcodes, Size originalImageSize)
//      : _barcodes = barcodes,
//        _originalImageSize = originalImageSize;
//
//  @override
//  BoxPainter createBoxPainter([VoidCallback onChanged]) {
//    return _BarcodeDetectPainter(_barcodes, _originalImageSize);
//  }
//}
//
//class _BarcodeDetectPainter extends BoxPainter {
//  final List<VisionBarcode> _barcodes;
//  final Size _originalImageSize;
//
//  _BarcodeDetectPainter(barcodes, originalImageSize)
//      : _barcodes = barcodes,
//        _originalImageSize = originalImageSize;
//
//  @override
//  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
//    final paint = Paint()
//      ..strokeWidth = 2.0
//      ..color = Colors.red
//      ..style = PaintingStyle.stroke;
//
//    final _heightRatio = _originalImageSize.height / configuration.size.height;
//    final _widthRatio = _originalImageSize.width / configuration.size.width;
//    for (var barcode in _barcodes) {
//      final _rect = Rect.fromLTRB(
//          offset.dx + barcode.rect.left / _widthRatio,
//          offset.dy + barcode.rect.top / _heightRatio,
//          offset.dx + barcode.rect.right / _widthRatio,
//          offset.dy + barcode.rect.bottom / _heightRatio);
//      canvas.drawRect(_rect, paint);
//    }
//    canvas.restore();
//  }
//}
//
//class TextDetectDecoration extends Decoration {
//  final Size _originalImageSize;
//  final List<VisionText> _texts;
//
//  TextDetectDecoration(List<VisionText> texts, Size originalImageSize)
//      : _texts = texts,
//        _originalImageSize = originalImageSize;
//
//  @override
//  BoxPainter createBoxPainter([VoidCallback onChanged]) {
//    return _TextDetectPainter(_texts, _originalImageSize);
//  }
//}
//
//class _TextDetectPainter extends BoxPainter {
//  final List<VisionText> _texts;
//  final Size _originalImageSize;
//
//  _TextDetectPainter(texts, originalImageSize)
//      : _texts = texts,
//        _originalImageSize = originalImageSize;
//
//  @override
//  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
//    final paint = Paint()
//      ..strokeWidth = 2.0
//      ..color = Colors.red
//      ..style = PaintingStyle.stroke;
//
//    final _heightRatio = _originalImageSize.height / configuration.size.height;
//    final _widthRatio = _originalImageSize.width / configuration.size.width;
//    for (var text in _texts) {
//      final _rect = Rect.fromLTRB(
//          offset.dx + text.rect.left / _widthRatio,
//          offset.dy + text.rect.top / _heightRatio,
//          offset.dx + text.rect.right / _widthRatio,
//          offset.dy + text.rect.bottom / _heightRatio);
//      canvas.drawRect(_rect, paint);
//    }
//    canvas.restore();
//  }
//}
