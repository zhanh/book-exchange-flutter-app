import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'colors.dart';

class Profile extends StatefulWidget {
  @override
  ProfileState createState() {
    return new ProfileState();
  }
}

class ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: kPaperWhite,
      body: StreamBuilder(
        stream: Firestore.instance.collection('books@exchange').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          try {
            if (!snapshot.hasData) return CircularProgressIndicator();
          } catch (e) {
            return SnackBar(
              content: Text(
                  'No internet connection. Unable to load data.'),
              duration: Duration(seconds: 5),
            );
          }
          return FsListView(documents: snapshot.data.documents);
        },
      ),
    );
  }
}

class FsListView extends StatelessWidget {
  final List<DocumentSnapshot> documents;

  FsListView({this.documents});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return ListView.builder(
        itemCount: documents.length,
        itemExtent: 170.0,
        itemBuilder: (BuildContext context, int index) {

          String title = documents[index].data['title'].toString();
          String author = documents[index].data['author'].toString();
          String isbn = documents[index].data['isbn'].toString();

          print('isbn: ' + isbn);

          return ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              title: Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.bottomRight,
                        end: new Alignment(0.1, 0.0),
                        colors: [
                          kPrimaryLightColorBrown,
                          kPrimaryDarkColorBrown,
                        ]),
                    borderRadius: BorderRadius.circular(5.0),
//                    border: Border.all(color: kPaleWhite),
//                    color: kPrimaryDarkColorBrown,
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 1.0,
                          offset: Offset(4.0, 4.0),
                          color: Colors.black38)
                    ]),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Container(
                      margin: EdgeInsets.fromLTRB(0.0, 0.0, 20.0, 0.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            title,
                            softWrap: true,
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(height: 20.0),
                          Text(author,
                              softWrap: true,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontStyle: FontStyle.italic)),
                        ],
                      ),
                    )),
                  ],
                ),
              ));
        });
  }
}
