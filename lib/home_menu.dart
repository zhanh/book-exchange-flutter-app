import 'package:flutter/material.dart';

import 'auth.dart';
import 'colors.dart';

class Menu extends StatelessWidget {
  final BaseAuth auth;
  final VoidCallback onSignedOut;

  Menu({
    @required this.auth,
    @required this.onSignedOut,
    Key key,
  }) : super(key: key);

  void _signOut() async {
    print("IN signout");
    try {
      await auth.signOut();
      onSignedOut();
    } catch (e) {
      print('${e.toString()} in Signing Out');
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Container(
        padding: EdgeInsets.all(12.0),
        color: kPrimaryColorAmber,
        child: ListView(
          padding: EdgeInsets.fromLTRB(0.0, 64.0, 0.0, 0.0),
          children: <Widget>[
            FlatButton(
              child: Text('HOME', style: TextStyle(color: kPrimaryTextColor)),
              onPressed: () {
                print('Go to Dashboard');
//                Navigator.push(
//                  context,
//                  MaterialPageRoute(builder: (BuildContext context) => Home()),
//                );
              },
            ),
            FlatButton(
              child: Text(
                'MY LIBRARY',
                style: TextStyle(color: kPrimaryTextColor),
              ),
              onPressed: () {
                print('Go to My Library');
//                Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                      builder: (BuildContext context) => MyLibrary()),
//                );
              },
            ),
            FlatButton(
              child:
                  Text('DISCOVER', style: TextStyle(color: kPrimaryTextColor)),
              onPressed: () {
                print('Go to Discover');
//                Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                      builder: (BuildContext context) => Discover()),
//                );
              },
            ),
            FlatButton(
              child:
                  Text('LOG OUT', style: TextStyle(color: kPrimaryTextColor)),
              onPressed: () {
                print('LOG OUT');
                _signOut();
//                Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                      builder: (BuildContext context) => Discover()),
//                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
