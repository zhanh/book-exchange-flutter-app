import 'dart:io';

import 'package:flutter/material.dart';

import 'colors.dart';
import 'main.dart';

class StartupPage extends StatefulWidget {
  StartupPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  StartupPageState createState() {
    return new StartupPageState();
  }
}

//enum InternetStatus { connected, disconnected }

class StartupPageState extends State<StartupPage> {
//  InternetStatus _internetStatus = InternetStatus.disconnected;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  BuildContext scaffoldContext;

  void _showError() {
    final snackBarFailure = SnackBar(
      content: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Text(
              "No internet connection.\nUnable to load application.",
              softWrap: true,
              maxLines: 3,
            ),
          ),
          RaisedButton(
              color: kErrorRed,
              splashColor: Colors.green,
              elevation: 2.0,
              onPressed: () {
                //add retry connection function

                RestartWidget.restartApp(context);
              },
              child: Text(
                'RETRY',
                textAlign: TextAlign.center,
              )),
        ],
      ),
      duration: Duration(minutes: 10),
    );
    Scaffold.of(scaffoldContext).showSnackBar(snackBarFailure);
  }

  void _showSuccess() {
    final snackBarSuccess = SnackBar(
      content: Text("Loading Application"),
      duration: Duration(seconds: 5),
    );
    Scaffold.of(scaffoldContext).showSnackBar(snackBarSuccess);
    Navigator.pushReplacementNamed(context, '/root_page');
  }

  @override
  void initState() {
    // TODO: implement initState
    testInternet();
    super.initState();
    print('testing Internet');
  }

  void testInternet() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
//        _internetStatus = InternetStatus.connected;
        _showSuccess();
      }
    } on SocketException catch (_) {
      print('not connected');
//      _internetStatus = InternetStatus.disconnected;
      _showError();
    }
  }

  //add button for restart widget near snackbar

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        backgroundColor: kPrimaryColorAmber,
        body: new Builder(
          builder: (BuildContext context) {
            scaffoldContext = context;
            return new Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Image.asset('assets/Icon.png'),
                    height: 80.0,
                    width: 80.0,
//                  color: Colors.black45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 5.0,
                            color: Colors.black12,
//                          offset: Offset(0.0, 4.0),
                            spreadRadius: 1.0)
                      ],
                    ),
                  ),
                  const SizedBox(height: 8.0),
                  const Text(
                    'BOOK EXCHANGE',
                    style: TextStyle(color: kSecondaryDarkColorBrown),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            );
          },
        ));
  }
}
