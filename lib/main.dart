import 'package:flutter/material.dart';

import 'app.dart';

//void main() => runApp(BookExchangeApp());

void main() {
  runApp(new RestartWidget(child: BookExchangeApp() // new MaterialApp,
      ));
}

class RestartWidget extends StatefulWidget {
  final Widget child;

  RestartWidget({this.child});

  static restartApp(BuildContext context) {
    final _RestartWidgetState state =
        context.ancestorStateOfType(const TypeMatcher<_RestartWidgetState>());
    state.restartApp();
  }

  @override
  _RestartWidgetState createState() => new _RestartWidgetState();
}

class _RestartWidgetState extends State<RestartWidget> {
  Key key = new UniqueKey();

  void restartApp() {
    this.setState(() {
      key = new UniqueKey();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      key: key,
      child: widget.child,
    );
  }
}
//void main() => runApp(new MyApp());
//
//class MyApp extends StatelessWidget {
//  const MyApp();
//
//  @override
//  Widget build(BuildContext context) {
//    return new MaterialApp(
//      title: 'Baby Names',
//      home: const MyHomePage(title: 'Baby Name Votes'),
//    );
//  }
//}
//
//class MyHomePage extends StatelessWidget {
//  const MyHomePage({Key key, this.title}) : super(key: key);
//
//  final String title;
//
//  Widget _buildListItem(BuildContext context, DocumentSnapshot document) {
//    var key = new ValueKey(document.documentID);
//
//    @override
//    Widget build(BuildContext context) {
//      return new Scaffold(
//        appBar: new AppBar(title: new Text(title)),
//        body: new StreamBuilder(
//            stream: Firestore.instance.collection('books@exchange').snapshots(),
//            builder: (context, snapshot) {
//              if (!snapshot.hasData) return CircularProgressIndicator();
//              return FirestoreListView(documents: snapshot.data.documents);
//            }),
//      );
//    }
//  }
//}

//class FirestoreListView extends StatelessWidget {
//  final List<DocumentSnapshot> documents;
//
//  FirestoreListView({this.documents});
//
//
////  Widget bookCard =
//
////  var card = Card(
////    margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
////    elevation: 0.0,
////    child: bookCard,
////  );
//
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return ListView.builder(
//        itemCount: documents.length,
//        itemExtent: 128.0,
//        itemBuilder: (BuildContext context, int index) {
//          String title = documents[index].data['title'].toString();
//          String author = documents[index].data['title'].toString();
//
//          return ListTile(
//            title: Container(
//              child: Row(
//                crossAxisAlignment: CrossAxisAlignment.center,
//                children: <Widget>[
//                  Container(
//                    color: kSecondaryLightColorBrown,
//                    width: 128.0,
//                    height: 128.0,
//                  ),
//                  Flexible(
//                    child: Column(
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Container(
//                            child: Text(
//                              title,
//                              softWrap: true,
//                              maxLines: 5,
//                              overflow: TextOverflow.ellipsis,
//                            ),
//                            padding: EdgeInsets.fromLTRB(
//                                16.0, 4.0, 16.0, 4.0)),
//                        SizedBox(height: 8.0),
//                        Container(
//                            child: Text(
//                              author,
//                              maxLines: 5,
//                              overflow: TextOverflow.ellipsis,
//                            ),
//                            padding: EdgeInsets.fromLTRB(
//                                16.0, 4.0, 16.0, 4.0))
//                      ],
//                    ),
//                  ),
//                ],
//              ),
//            ),
//          );
//        }
//    );
//  }
//}

//
//class My_Library extends StatelessWidget {
//  const My_Library();
//
//  @override
//  Widget build(BuildContext context) {
//    return new MaterialApp(
////      title: 'Baby Names',
//      home: const MyHomePage(title: 'My Library'),
//    );
//  }
//}

//class MyLibrary extends StatefulWidget {
//  @override
//  State<StatefulWidget> createState() {
//    return new MyLibraryState();
//  }
//}

//class MyLibraryState extends State<MyLibrary> {
//  var _isLoading = true;
//
//  Widgest _buildListItem(BuildContext context, DocumentSnapshot document) {
//    return ListTile(
//      title: Row()
//    )
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return new MaterialApp(
//      home: new Scaffold(
//          appBar: new AppBar(
//            title: new Text("My Library"),
//            actions: <Widget>[
//              new IconButton(
//                  icon: new Icon(Icons.refresh),
//                  onPressed: () {
//                    print("Reloading...");
//                  })
//            ],
//          ),
//          body: StreamBuilder(
//              stream: FireStore.instance.collection('mybooks').snapshots(),
//              builder: (context, snapshot) {
//                if (!snapshot.hasData) return const Text('Loading...');
//                return ListView.builder(
//                  itemExtent: 80.0,
//                  itemCount: snapshot.data.documents.length,
//                  itemBuilder: (context, index) =>
//                      _buildListItem(context, snapshot.data.documents[index]),
//                )
//              }
//          )),
////          body: new Center(
////            child: _isLoading
////                ? new CircularProgressIndicator()
////                : new Text("Finished Loading..."),
////          )),
//    );
//  }
//}

//class MyApp extends StatelessWidget {
//  // This widget is the root of your application.x
//  @override
//  Widget build(BuildContext context) {
//    return new MaterialApp(
//      title: 'Flutter Demo',
//      theme: new ThemeData(
//        // This is the theme of your application.
//        //
//        // Try running your application with "flutter run". You'll see the
//        // application has a blue toolbar. Then, without quitting the app, try
//        // changing the primarySwatch below to Colors.green and then invoke
//        // "hot reload" (press "r" in the console where you ran "flutter run",
//        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
//        // counter didn't reset back to zero; the application is not restarted.
//        primarySwatch: Colors.blue,
//      ),
//      home: new MyHomePage(title: 'Book Swap'),
//    );
//  }
//}
//
//class MyHomePage extends StatefulWidget {
//  MyHomePage({Key key, this.title}) : super(key: key);
//
//  // This widget is the home page of your application. It is stateful, meaning
//  // that it has a State object (defined below) that contains fields that affect
//  // how it looks.
//
//  // This class is the configuration for the state. It holds the values (in this
//  // case the title) provided by the parent (in this case the App widget) and
//  // used by the build method of the State. Fields in a Widget subclass are
//  // always marked "final".
//
//  final String title;
//
//  @override
//  _MyHomePageState createState() => new _MyHomePageState();
//}
//
//class _MyHomePageState extends State<MyHomePage> {
//  int _counter = 0;
//
//  void _incrementCounter() {
//    setState(() {
//      // This call to setState tells the Flutter framework that something has
//      // changed in this State, which causes it to rerun the build method below
//      // so that the display can reflect the updated values. If we changed
//      // _counter without calling setState(), then the build method would not be
//      // called again, and so nothing would appear to happen.
//      _counter++;
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    // This method is rerun every time setState is called, for instance as done
//    // by the _incrementCounter method above.
//    //
//    // The Flutter framework has been optimized to make rerunning build methods
//    // fast, so that you can just rebuild anything that needs updating rather
//    // than having to individually change instances of widgets.
//    return new Scaffold(
//      appBar: new AppBar(
//        // Here we take the value from the MyHomePage object that was created by
//        // the App.build method, and use it to set our appbar title.
//        title: new Text(widget.title),
//      ),
//      body: new Center(
//        // Center is a layout widget. It takes a single child and positions it
//        // in the middle of the parent.
//        child: new Column(
//          // Column is also layout widget. It takes a list of children and
//          // arranges them vertically. By default, it sizes itself to fit its
//          // children horizontally, and tries to be as tall as its parent.
//          //
//          // Invoke "debug paint" (press "p" in the console where you ran
//          // "flutter run", or select "Toggle Debug Paint" from the Flutter tool
//          // window in IntelliJ) to see the wireframe for each widget.
//          //
//          // Column has various properties to control how it sizes itself and
//          // how it positions its children. Here we use mainAxisAlignment to
//          // center the children vertically; the main axis here is the vertical
//          // axis because Columns are vertical (the cross axis would be
//          // horizontal).
//          mainAxisAlignment: MainAxisAlignment.center,
//          children: <Widget>[
//            new Text(
//              'You have pushed the button this many times:',
//            ),
//            new Text(
//              '$_counter',
//              style: Theme.of(context).textTheme.display1,
//            ),
//          ],
//        ),
//      ),
//      floatingActionButton: new FloatingActionButton(
//        onPressed: _incrementCounter,
//        tooltip: 'Increment',
//        child: new Icon(Icons.add),
//      ), // This trailing comma makes auto-formatting nicer for build methods.
//    );
//  }
//}
