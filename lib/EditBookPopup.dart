import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

import 'colors.dart';

class EditBookPopup extends StatefulWidget {
  const EditBookPopup({
    this.uid,
    this.title,
    this.author,
    this.isbn13,
    this.list,
    this.docRef,
  });

  final String uid;
  final String title;
  final String author;
  final String isbn13;
  final bool list;
  final DocumentReference docRef;

  @override
  EditBookPopupState createState() {
    print('IN ADDBOOKPOPUP STATE uid: $uid');
    return new EditBookPopupState();
  }
}

enum SearchStatus {
  searching,
  none,
}

class EditBookPopupState extends State<EditBookPopup> {
  String _path;
  File _cachedFile;

  SearchStatus searchStatus = SearchStatus.none;

  Future<Null> downloadFile(String httpPath) async {
    final RegExp regExp = RegExp('([^?/]*\.(jpg))');
    final String fileName = regExp.stringMatch(httpPath);
    final Directory tempDir = Directory.systemTemp;
    final File file = File('${tempDir.path}/$fileName');

    final StorageReference ref = FirebaseStorage.instance.ref().child(fileName);
    final StorageFileDownloadTask downloadTask = ref.writeToFile(file);

    final int byteNumber = (await downloadTask.future).totalByteCount;

//    print(byteNumber);

    setState(() => _cachedFile = file);
  }

  Future<Null> uploadFile(String filepath) async {
    final ByteData bytes = await rootBundle.load(filepath);
    final Directory tempDir = Directory.systemTemp;
    final String fileName = "${Random().nextInt(10000)}.jpg";
    final File file = File('${tempDir.path}/$fileName');
    file.writeAsBytes(bytes.buffer.asInt8List(), mode: FileMode.write);

    final StorageReference ref = FirebaseStorage.instance.ref().child(fileName);
    final StorageUploadTask task = ref.putFile(file);
    final Uri downloadUrl = (await task.future).downloadUrl;
    _path = downloadUrl.toString();

//    print(_path);
  }

  final myController = TextEditingController();
  Widget appBarTitle =
      new Text('BOOK DETAILS', style: TextStyle(color: kPaperWhite));
  Icon actionIcon = new Icon(Icons.search, color: kPaperWhite);
  Icon leadingIcon;

//  String isbnSubmitted = "";
  var isbnSearched;
  var isbn13;

  Widget listOnExchange() {
    return FlatButton(
        color: Colors.green,
        child: Center(
            child: Text('LIST ON\nEXCHANGE',
                softWrap: true,
                maxLines: 3,
                style: TextStyle(color: kPaperWhite))),
        onPressed: () {
          return showDialog<Null>(
            context: context,
            barrierDismissible: false, // user must tap button!
            builder: (BuildContext context) {
              return new AlertDialog(
                title: new Text(
                  'LIST ON EXCHANGE',
                  style: TextStyle(color: kPaperWhite),
                ),
                content: new SingleChildScrollView(
                  child: new ListBody(
                    children: <Widget>[
                      new Text(
                        'Do you want to sell your book?',
                        style: TextStyle(color: kPaperWhite),
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'CANCEL',
                        style: TextStyle(color: kPaperWhite),
                      )),
                  RaisedButton(
                    color: Colors.green,
                    onPressed: () {
                      Firestore.instance
                          .runTransaction((Transaction transaction) async {
                        DocumentSnapshot snapshot =
                            await transaction.get(widget.docRef);
                        await transaction.update(
                            snapshot.reference, ({'list': true}));
                      });

                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'CONFIRM',
                      style: TextStyle(color: kPaperWhite),
                    ),
                  ),
                ],
              );
            },
          );

//  Navigator.push(context, route)
        });
  }

  Widget delistFromExchange() {
    return FlatButton(
        color: Colors.redAccent,
        child: Center(
            child: Text('DELIST FROM\nEXCHANGE',
                softWrap: true,
                maxLines: 3,
                style: TextStyle(color: kPaperWhite))),
        onPressed: () {
          return showDialog<Null>(
            context: context,
            barrierDismissible: false, // user must tap button!
            builder: (BuildContext context) {
              return new AlertDialog(
                title: new Text(
                  'DELIST FROM EXCHANGE',
                  style: TextStyle(color: kPaperWhite),
                ),
                content: new SingleChildScrollView(
                  child: new ListBody(
                    children: <Widget>[
                      new Text(
                        'Do you want to delist your book from the exchange?',
                        style: TextStyle(color: kPaperWhite),
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'CANCEL',
                        style: TextStyle(color: kPaperWhite),
                      )),
                  RaisedButton(
                    color: Colors.green,
                    onPressed: () {
                      Firestore.instance
                          .runTransaction((Transaction transaction) async {
                        DocumentSnapshot snapshot =
                            await transaction.get(widget.docRef);
                        await transaction.update(
                            snapshot.reference, ({'list': false}));
                      });

                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'CONFIRM',
                      style: TextStyle(color: kPaperWhite),
                    ),
                  ),
                ],
              );
            },
          );

//  Navigator.push(context, route)
        });
  }

  Widget bookDetails() {
    return ListView.builder(
        itemCount: 1,
        itemExtent: 100.0,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            contentPadding:
                EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
            title: Container(
              margin: EdgeInsets.symmetric(horizontal: 10.0),
//              decoration: BoxDecoration(
//                  gradient: LinearGradient(
//                      begin: Alignment.bottomRight,
//                      end: new Alignment(0.1, 0.0),
//                      colors: [
//                        kPrimaryDarkColorBrown,
//                        kSecondaryDarkColorBrown,
//                      ]),
//                  borderRadius: BorderRadius.circular(5.0),
//                  boxShadow: [
//                    BoxShadow(
//                        blurRadius: 1.0,
//                        offset: Offset(4.0, 4.0),
//                        color: Colors.black38)
//                  ]),
              child: Row(
                children: <Widget>[
                  Expanded(
//                      margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
//                      color: kPrimaryDarkColorBrown,
                    child: StreamBuilder<QuerySnapshot>(
                        stream: Firestore.instance
                            .collection('users/${widget.uid}/library')
                            .snapshots(),
                        builder: (BuildContext context,
                            AsyncSnapshot<QuerySnapshot> snapshot) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.none:
                              return SnackBar(
                                content: Row(
                                  children: <Widget>[
                                    Text(
                                        'No internet connection. Unable to load data.'),
                                    FlatButton(
                                        onPressed: () {
                                          //TODO REFRESH PAGE FUNCTION
                                        },
                                        child: Text('TAP TO RETRY'))
                                  ],
                                ),
                                duration: Duration(seconds: 5),
                              );
                            case ConnectionState.waiting:
                              return Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 40.0, vertical: 55.0),
                                  child: CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            kPrimaryColorAmber),
                                  ));
                              break;
                            default:
                              if (snapshot.hasError) {
                              } else {
//                                isbn13 = document['isbn13'];
                                print('title TEST: ' + widget.title.toString());
//                                print('author TEST: ' + isbn13.toString());
//                                print('isbn13 TEST: ' + isbn13.toString());

                                return Row(
                                  children: <Widget>[
//                                    Container(
//                                        width: 108.0,
//                                        decoration: BoxDecoration(
//                                            border:
//                                                Border.all(color: kPaperWhite)),
//                                        child: Image.network(
//                                          widget.smallThumbnailUrl,
//                                          fit: BoxFit.cover,
//                                        )),
                                    Expanded(
                                        child: Container(
                                      margin: EdgeInsets.fromLTRB(
                                          0.0, 0.0, 20.0, 12.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          Text(
                                            widget.title,
                                            softWrap: true,
                                            maxLines: 3,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              color: kSecondaryDarkColorBrown,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                          SizedBox(height: 4.0),
                                          Text(widget.author,
                                              softWrap: true,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color:
                                                      kSecondaryDarkColorBrown,
                                                  fontStyle: FontStyle.italic)),
                                          SizedBox(height: 4.0),
                                          Text(widget.isbn13,
                                              softWrap: true,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color:
                                                      kSecondaryDarkColorBrown,
                                                  fontStyle: FontStyle.italic)),
                                        ],
                                      ),
                                    )),
                                    widget.list
                                        ? delistFromExchange()
                                        : listOnExchange(),
                                  ],
                                );
                              }
                          }
                        }),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kPrimaryColorAmber,
        appBar: AppBar(title: appBarTitle),
        body: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  margin:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                  height: 100.0,
                  width: 100.0,
//                      color: kDarkGrey,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    border: Border.all(color: kPaperWhite),
                    color: kDarkGrey,
                  ),
                  child: Column(children: <Widget>[
                    IconButton(icon: Icon(Icons.add), onPressed: null),
                    Text(
                      'Front\nCover',
                      textAlign: TextAlign.center,
                    ),
                  ]),
                ),
                Container(
                  margin:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                  height: 100.0,
                  width: 100.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    border: Border.all(color: kPaperWhite),
                    color: kDarkGrey,
                  ),
                  child: Column(children: <Widget>[
                    IconButton(icon: Icon(Icons.add), onPressed: null
//                      (){
//                      Navigator.push(context, MaterialPageRoute(builder: (context) => ))
//                    }
                        ),
                    Text(
                      'Back\nCover',
                      textAlign: TextAlign.center,
                    ),
                  ]),
                ),
                Container(
                  margin:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                  height: 100.0,
                  width: 100.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    border: Border.all(color: kPaperWhite),
                    color: kDarkGrey,
                  ),
                  child: Column(children: <Widget>[
                    IconButton(icon: Icon(Icons.add), onPressed: null),
                    Text(
                      'Inner\nPages',
                      textAlign: TextAlign.center,
                    ),
                  ]),
                ),
              ],
            ),
//            SizedBox(height: 20.0),
            Expanded(
              child: Scaffold(
                  backgroundColor: kPaperWhite,
                  body: Column(
                    children: <Widget>[
                      SizedBox(height: 20.0),
                      Text(
                        'BOOK DETAILS',
                        style: TextStyle(color: kSecondaryDarkColorBrown),
                      ),
                      SizedBox(height: 10.0),
//                      SizedBox(height: 20.0, child: C,),
                      Container(
                        height: 0.5,
//                        color: kDarkGrey,
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                              blurRadius: 1.0,
                              offset: Offset(4.0, 4.0),
                              color: Colors.black38)
                        ]),
                      ),
                      SizedBox(height: 10.0),

                      Expanded(
                        child: bookDetails(),
                      ),
//                      ButtonBar(
//                          mainAxisSize: MainAxisSize.max,
//                          alignment: MainAxisAlignment.center,
//                          children: <Widget>[
//                            FlatButton(
//                                color: Colors.green,
//                                child: Text(
//                                  'LIST ON EXCHANGE',
//                                  softWrap: true,
//                                  maxLines: 2,
//                                  style: TextStyle(
//                                    color: kPaperWhite,
//                                  ),
//                                ),
//                                onPressed: () {
////                        Navigator.push(context, route)
//                                }),
//                          ]),

                      SizedBox(height: 10.0),
                      //TODO INSERT SEARCH RESULT FROM GOOGLEBOOKS
                    ],
                  )),
            )
          ],
        ),
        bottomNavigationBar: ButtonBar(
          children: <Widget>[
            FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  'CANCEL',
                  style: TextStyle(color: kSecondaryDarkColorBrown),
                )),
            RaisedButton(
                child: Text('DONE'),
                onPressed: () {
                  Firestore.instance
                      .runTransaction((Transaction transaction) async {
                    final CollectionReference reference = Firestore.instance
                        .collection('users/${widget.uid}/library');

//                    await reference.add({
//                      'title': title,
//                      'author': author,
//                      'isbn13': isbn13,
//                      'timestamp': DateTime.now(),
//                      'public': 'false'
//                    });
                  });
                  Navigator.pop(context);
                }),
          ],
        ));
  }
}
