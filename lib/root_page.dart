import 'package:flutter/material.dart';

import 'auth.dart';
import 'colors.dart';
import 'discover.dart';
import 'discover_backdrop.dart';
import 'home.dart';
import 'home_backdrop.dart';
import 'login_page.dart';
import 'menu.dart';
import 'my_library.dart';
import 'my_library_backdrop.dart';

class RootPage extends StatefulWidget {
  final BaseAuth auth;

  RootPage({this.auth});

  @override
  State<StatefulWidget> createState() => new _RootPageState();
}

enum AuthStatus { notSignedIn, signedIn }

class _RootPageState extends State<RootPage> {
  AuthStatus authStatus = AuthStatus.notSignedIn;

  int currentTab = 0;

  List<Widget> pages;
  List<String> frontPagesTitle = ["HOME", "MY LIBRARY", "WISHLIST", "DISCOVER"];
  List<Key> frontPagesKey;
  Widget currentPage;
  String currentPageTitle;
  Home home;
  MyLibrary myLibrary;
  Discover discover;
  Menu menu;
  HomeBackdrop homeBackdrop;
  MyLibraryBackdrop myLibraryBackdrop;
  DiscoverBackdrop discoverBackdrop;
  Key currentPageKey;

  final Key keyHome = PageStorageKey('home');
  final Key keyMyLibrary = PageStorageKey('myLibrary');
  final Key keyDiscover = PageStorageKey('discover');
  final Key keyMenu = PageStorageKey('menu');

  final PageStorageBucket bucket = PageStorageBucket();

  //initState is not asynchronous
  @override
  void initState() {
    // TODO: implement initState

//    print(widget.auth.currentUser().);
    widget.auth.currentUser().then((userId) {
      setState(() {
        authStatus =
            userId == null ? AuthStatus.notSignedIn : AuthStatus.signedIn;
      });
    });

    home = Home(
      auth: widget.auth,
    );

    myLibrary = MyLibrary(
      auth: widget.auth,
//      key: keyMyLibrary,
    );

    discover = Discover(
//      key: keyDiscover,
      auth: widget.auth,
    );

    menu = Menu(
        auth: widget.auth,
        onSignedOut: _signedOut); //Menu needs to be tailored for each page

    homeBackdrop = HomeBackdrop(
        key: keyHome,
        frontLayer: home,
        backLayer: menu,
        frontTitle: Text('HOME'),
        backTitle: Text('MENU'));

    myLibraryBackdrop = MyLibraryBackdrop(
        key: keyMyLibrary,
        frontLayer: myLibrary,
        backLayer: menu,
        frontTitle: Text("MY LIBRARY"),
        backTitle: Text('MENU'),
        auth: widget.auth);

    discoverBackdrop = DiscoverBackdrop(
        key: keyDiscover,
        frontLayer: discover,
        backLayer: menu,
        frontTitle: Text('DISCOVER'),
        backTitle: Text('MENU'));

    pages = [
      homeBackdrop,
      myLibraryBackdrop,
      myLibraryBackdrop,
      discoverBackdrop
    ];

    frontPagesKey = [keyHome, keyMyLibrary, keyMyLibrary, keyDiscover];

    currentPage = pages[0];
    currentPageTitle = frontPagesTitle[0];
    currentPageKey = frontPagesKey[0];
    super.initState();
  }

  void _signedIn() {
    print("IN SIGNED IN VOID CALLBACK");
    setState(() {
      authStatus = AuthStatus.signedIn;
    });
  }

  void _signedOut() {
    print("IN SIGNED IN VOID CALLBACK");

    setState(() {
      authStatus = AuthStatus.notSignedIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.signedIn:
        return Scaffold(
          backgroundColor: kPrimaryColorAmber,
          bottomNavigationBar: new Theme(
            data: Theme.of(context).copyWith(
                // sets the background color of the `BottomNavigationBar`
                canvasColor: kSecondaryDarkColorBrown,
                // sets the active color of the `BottomNavigationBar` if `Brightness` is light
                primaryColor: Colors.red,
                textTheme: Theme
                    .of(context)
                    .textTheme
                    .copyWith(caption: new TextStyle(color: kPaperWhite))),
            // sets the inactive color of the `BottomNavigationBar`
            child: BottomNavigationBar(
              currentIndex: currentTab,
              onTap: (int index) {
                setState(() {
                  currentTab = index;
                  currentPage = pages[index];
                  currentPageTitle = frontPagesTitle[index];
                  currentPageKey = frontPagesKey[index];
                });
              },
              type: BottomNavigationBarType.fixed,
              items: [
                BottomNavigationBarItem(
                    icon: Icon(Icons.home), title: Text('HOME')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.folder_special),
                    title: new Text('MY LIBRARY')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.favorite), title: Text('WISHLIST')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.search), title: Text('DISCOVER')),
              ],
            ),
          ),
          body: PageStorage(
            child: currentPage,
            bucket: bucket,
            key: currentPageKey,
          ),
        );
      default:
        return LoginPage(auth: widget.auth, onSignedIn: _signedIn);
    }
  }
}
