import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import './EditBookPopup.dart';
import './model/book.dart';
import 'auth.dart';
import 'colors.dart';

class MyLibrary extends StatefulWidget {
  const MyLibrary({
    Key key,
    this.auth,
  }) : super(key: key);

  final BaseAuth auth;

  @override
  State<StatefulWidget> createState() {
    return new _MyLibraryState();
  }
}

class _MyLibraryState extends State<MyLibrary> {
  String _uid;

  void initState() {
    widget.auth.currentUser().then((userId) {
      setState(() {
        userId == null
            ? print("ERROR IN INIT STATE LIBRARY USERID NULL")
            : _uid = userId.uid.toString();
        print('USERID IN INIT STATE: $_uid');
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
        backgroundColor: kPaperWhite,
        body: StreamBuilder(
          stream:
              Firestore.instance.collection('users/$_uid/library').snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) return CircularProgressIndicator();
            return FirestoreListView(
                documents: snapshot.data.documents, uid: _uid);
          },
        ));
  }
}

class FirestoreListView extends StatelessWidget {
  final List<DocumentSnapshot> documents;
  final String uid;

  FirestoreListView({this.documents, this.uid});

  Widget showStatus(bool list) {
    if (list) {
      return Icon(Icons.public);
    }

    return Icon(Icons.lock);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print("IN BUILD");
    return ListView.builder(
        itemCount: documents.length,
        itemExtent: 170.0,
        itemBuilder: (BuildContext context, int index) {
          var title = documents[index].data['title'];
          var author = documents[index].data['author'];
          var isbn13 = documents[index].data['isbn13'];
          bool list = documents[index].data['list'];

          print("title $title");
          print("author $author");
          print("isbn13 $isbn13");
          print("visibility $list");

          Future<Book> fetchPost() async {
            var uri = 'https://www.googleapis.com/books/v1/volumes?q=' + isbn13;
            print("fetching response");
            print('uri: ' + uri);
            final response = await http.get(uri);
            if (response.statusCode == 200) {
              return Book.fromJson(json.decode(response.body));
            } else {
              throw Exception('Error: Failed to load');
            }
          }

          return ListTile(
            onTap: () {
              Navigator.push(
                context,
                EditBookRoute(
                    widget: EditBookPopup(
                  uid: uid,
                  title: title,
                  author: author,
                  isbn13: isbn13,
                  list: list,
                  docRef: documents[index].reference,
                )),
              );
            },
            contentPadding:
                EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
            title: Container(
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.bottomRight,
                      end: new Alignment(0.1, 0.0),
                      colors: [
                        kPrimaryDarkColorBrown,
                        kSecondaryDarkColorBrown,
                      ]),
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 1.0,
                        offset: Offset(4.0, 4.0),
                        color: Colors.black38)
                  ]),
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
                    width: 108.0,
                    color: kPrimaryDarkColorBrown,
                    child: FutureBuilder<Book>(
                        future: fetchPost(),
                        builder: (BuildContext context,
                            AsyncSnapshot<Book> snapshot) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.none:
                              return SnackBar(
                                content: Row(
                                  children: <Widget>[
                                    Text(
                                        'No internet connection. Unable to load data.'),
                                    FlatButton(
                                        onPressed: () {
                                          //TODO REFRESH PAGE FUNCTION
                                        },
                                        child: Text('TAP TO RETRY'))
                                  ],
                                ),
                                duration: Duration(seconds: 5),
                              );
                            case ConnectionState.waiting:
                              return Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 40.0, vertical: 55.0),
                                  child: CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            kPrimaryColorAmber),
                                  ));
                              break;
                            default:
                              if (snapshot.hasError) {
                                print('Error: in error snapshot' +
                                    snapshot.error);
                                return new Container(
                                  width: 140.0,
                                  height: 160.0,
                                  color: kprimaryLightColorAmber,
                                  child: Icon(
                                    Icons.broken_image,
                                    color: kPrimaryDarkColorBrown,
                                  ),
                                );
                              } else {
                                String smallThumbnailUrl =
                                    snapshot.data.smallThumbnailUrl;
                                print('image url: ' +
                                    smallThumbnailUrl.toString());

                                return Container(
                                    decoration: BoxDecoration(
                                        border: Border.all(color: kPaperWhite)),
                                    child: Image.network(
                                      smallThumbnailUrl,
                                      fit: BoxFit.cover,
                                    ));
                              }
                          }
                        }),
                  ),
                  Expanded(
                      child: Container(
                    margin: EdgeInsets.fromLTRB(0.0, 0.0, 8.0, 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        showStatus(list),
                        Text(
                          title,
                          softWrap: true,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(height: 4.0),
                        Text(author,
                            softWrap: true,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontStyle: FontStyle.italic)),
                      ],
                    ),
                  )),
                  IconButton(
                    icon: Icon(Icons.cancel),
                    onPressed: () => Firestore.instance
                            .runTransaction((Transaction transaction) async {
                          DocumentSnapshot snapshot =
                              await transaction.get(documents[index].reference);

                          await transaction.delete(snapshot.reference);
                        }),
                  ),
                ],
              ),
            ),
          );
        });
  }
}

class EditBookRoute extends PageRouteBuilder {
  final Widget widget;

  EditBookRoute({this.widget})
      : super(pageBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secondaryAnimation) {
          return widget;
        }, transitionsBuilder: (BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child) {
          return new ScaleTransition(
            scale: new Tween<double>(
              begin: 0.0,
              end: 1.0,
            ).animate(
              CurvedAnimation(
                parent: animation,
                curve: Interval(
                  0.00,
                  0.50,
                  curve: Curves.linear,
                ),
              ),
            ),
            child: ScaleTransition(
              scale: Tween<double>(
                begin: 1.5,
                end: 1.0,
              ).animate(
                CurvedAnimation(
                  parent: animation,
                  curve: Interval(
                    0.50,
                    1.00,
                    curve: Curves.linear,
                  ),
                ),
              ),
              child: child,
            ),
          );
        });
}
