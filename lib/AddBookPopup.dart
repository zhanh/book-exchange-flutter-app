import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:http/http.dart' as http;

import './model/book.dart';
import 'colors.dart';

class AddBookPopup extends StatefulWidget {
  const AddBookPopup({
    this.uid,
  });

  final String uid;

  @override
  AddBookPopupState createState() {
    print('IN ADDBOOKPOPUP STATE uid: $uid');
    return new AddBookPopupState();
  }
}

enum SearchStatus {
  searching,
  none,
}

class AddBookPopupState extends State<AddBookPopup> {
  String _path;
  File _cachedFile;

  SearchStatus searchStatus = SearchStatus.none;

  Future<Null> downloadFile(String httpPath) async {
    final RegExp regExp = RegExp('([^?/]*\.(jpg))');
    final String fileName = regExp.stringMatch(httpPath);
    final Directory tempDir = Directory.systemTemp;
    final File file = File('${tempDir.path}/$fileName');

    final StorageReference ref = FirebaseStorage.instance.ref().child(fileName);
    final StorageFileDownloadTask downloadTask = ref.writeToFile(file);

    final int byteNumber = (await downloadTask.future).totalByteCount;

//    print(byteNumber);

    setState(() => _cachedFile = file);
  }

  Future<Null> uploadFile(String filepath) async {
    final ByteData bytes = await rootBundle.load(filepath);
    final Directory tempDir = Directory.systemTemp;
    final String fileName = "${Random().nextInt(10000)}.jpg";
    final File file = File('${tempDir.path}/$fileName');
    file.writeAsBytes(bytes.buffer.asInt8List(), mode: FileMode.write);

    final StorageReference ref = FirebaseStorage.instance.ref().child(fileName);
    final StorageUploadTask task = ref.putFile(file);
    final Uri downloadUrl = (await task.future).downloadUrl;
    _path = downloadUrl.toString();

//    print(_path);
  }

  final myController = TextEditingController();
  Widget appBarTitle = new Text(
    'ADD A BOOK',
    style: TextStyle(color: kPaperWhite),
  );
  Icon actionIcon = new Icon(Icons.search, color: kPaperWhite);
  Icon leadingIcon;

//  String isbnSubmitted = "";
  var isbnSearched;
  var smallThumbnailUrl;
  var title;
  var author;
  var isbn13;

  void setISBN(String isbnSubmitted) {
    isbnSearched = isbnSubmitted;
  }

  Widget searchedBook() {
    // TODO: implement build
    print("IN BUILD");
    print('searchStatus == $searchStatus');
    if (searchStatus == SearchStatus.searching) {
      print('BUILDING LIST');
      print('ISBN SUBMITTED IN SEARCHED BOOK : $isbnSearched');
      return ListView.builder(
          itemCount: 1,
          itemExtent: 170.0,
          itemBuilder: (BuildContext context, int index) {
            Future<Book> fetchPost() async {
              var uri = 'https://www.googleapis.com/books/v1/volumes?q=' +
                  isbnSearched;
              print('URI tEST: $uri');
              final response = await http.get(uri);
              if (response.statusCode == 200) {
                return Book.fromJson(json.decode(response.body));
              } else {
                throw Exception('Error: Failed to load');
              }
            }

            return ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              title: Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.bottomRight,
                        end: new Alignment(0.1, 0.0),
                        colors: [
                          kPrimaryDarkColorBrown,
                          kSecondaryDarkColorBrown,
                        ]),
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 1.0,
                          offset: Offset(4.0, 4.0),
                          color: Colors.black38)
                    ]),
                child: Row(
                  children: <Widget>[
                    Expanded(
//                      margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
//                      color: kPrimaryDarkColorBrown,
                      child: FutureBuilder<Book>(
                          future: fetchPost(),
                          builder: (BuildContext context,
                              AsyncSnapshot<Book> snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.none:
                                return SnackBar(
                                  content: Row(
                                    children: <Widget>[
                                      Text(
                                          'No internet connection. Unable to load data.'),
                                      FlatButton(
                                          onPressed: () {
                                            //TODO REFRESH PAGE FUNCTION
                                          },
                                          child: Text('TAP TO RETRY'))
                                    ],
                                  ),
                                  duration: Duration(seconds: 5),
                                );
                              case ConnectionState.waiting:
                                return Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 40.0, vertical: 55.0),
                                    child: CircularProgressIndicator(
                                      valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              kPrimaryColorAmber),
                                    ));
                                break;
                              default:
                                if (snapshot.hasError) {
                                  print('Error: in error snapshot' +
                                      snapshot.error);
                                  return new Container(
                                    width: 140.0,
                                    height: 160.0,
                                    color: kprimaryLightColorAmber,
                                    child: Icon(
                                      Icons.broken_image,
                                      color: kPrimaryDarkColorBrown,
                                    ),
                                  );
                                } else {
                                  smallThumbnailUrl =
                                      snapshot.data.smallThumbnailUrl;
                                  print('image url: ' +
                                      smallThumbnailUrl.toString());

                                  title = snapshot.data.title;
                                  print('title: ' + title.toString());

                                  author = snapshot.data.author;

                                  isbn13 = snapshot.data.isbn13;
                                  print('isbn13 TEST: ' + isbn13.toString());
                                  return Row(
                                    children: <Widget>[
                                      Container(
                                          width: 108.0,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: kPaperWhite)),
                                          child: Image.network(
                                            smallThumbnailUrl,
                                            fit: BoxFit.cover,
                                          )),
                                      Expanded(
                                          child: Container(
                                        margin: EdgeInsets.fromLTRB(
                                            0.0, 0.0, 20.0, 12.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: <Widget>[
                                            Text(
                                              title,
                                              softWrap: true,
                                              maxLines: 3,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                color: kPaperWhite,
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                            SizedBox(height: 4.0),
                                            Text(author,
                                                softWrap: true,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    color: kPaperWhite,
                                                    fontStyle:
                                                        FontStyle.italic)),
                                            SizedBox(height: 4.0),
                                            Text(isbn13,
                                                softWrap: true,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    color: kPaperWhite,
                                                    fontStyle:
                                                        FontStyle.italic)),
                                          ],
                                        ),
                                      )),
                                    ],
                                  );
                                }
                            }
                          }),
                    ),
                  ],
                ),
              ),
            );
          });
    } else {
      return Text('Search book by selecting the search icon',
          style: TextStyle(color: kSecondaryDarkColorBrown));
    }
  }

  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kPrimaryColorAmber,
        appBar:
            AppBar(title: appBarTitle, leading: leadingIcon, actions: <Widget>[
          new IconButton(
            icon: actionIcon,
            onPressed: () {
              setState(() {
                if (this.actionIcon.icon == Icons.search) {
                  this.actionIcon = new Icon(
                    Icons.close,
                    color: kPaperWhite,
                  );
                  this.leadingIcon = Icon(Icons.search, color: kPaperWhite);
                  this.appBarTitle = new TextField(
                    onSubmitted: (String isbnSubmitted) {
                      searchStatus = SearchStatus.searching;
//                      print('ISBN SUBMITTED: $isbnSubmitted');
                      setState(() {
                        setISBN(isbnSubmitted.toString());

                        this.leadingIcon = null;
                        this.actionIcon = new Icon(
                          Icons.search,
                          color: kPaperWhite,
                        );
                        this.appBarTitle = new Text(
                          'ADD A BOOK',
                          style: TextStyle(color: kPaperWhite),
                        );
                      });

                      myController.text = "";
                    },
                    controller: myController,
                    textAlign: TextAlign.left,
                    style: new TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                    ),
                    decoration: new InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                        border: InputBorder.none,
//                        prefixIcon: new Icon(Icons.search, color: kPaperWhite),
                        hintText: "Enter ISBN...",
                        hintStyle: new TextStyle(color: Colors.white)),
                  );

//                  isbn = myController.text;
                } else {
                  this.leadingIcon = null;
                  this.actionIcon = new Icon(
                    Icons.search,
                    color: kPaperWhite,
                  );
                  this.appBarTitle = new Text(
                    'ADD A BOOK',
                    style: TextStyle(color: kPaperWhite),
                  );
                }
              });
            },
          ),
        ]),
        body: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  margin:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                  height: 100.0,
                  width: 100.0,
//                      color: kDarkGrey,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    border: Border.all(color: kPaperWhite),
                    color: kDarkGrey,
                  ),
                  child: Column(children: <Widget>[
                    IconButton(icon: Icon(Icons.add), onPressed: null),
                    Text(
                      'Front\nCover',
                      textAlign: TextAlign.center,
                    ),
                  ]),
                ),
                Container(
                  margin:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                  height: 100.0,
                  width: 100.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    border: Border.all(color: kPaperWhite),
                    color: kDarkGrey,
                  ),
                  child: Column(children: <Widget>[
                    IconButton(icon: Icon(Icons.add), onPressed: null
//                      (){
//                      Navigator.push(context, MaterialPageRoute(builder: (context) => ))
//                    }
                        ),
                    Text(
                      'Back\nCover',
                      textAlign: TextAlign.center,
                    ),
                  ]),
                ),
                Container(
                  margin:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                  height: 100.0,
                  width: 100.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    border: Border.all(color: kPaperWhite),
                    color: kDarkGrey,
                  ),
                  child: Column(children: <Widget>[
                    IconButton(icon: Icon(Icons.add), onPressed: null),
                    Text(
                      'Inner\nPages',
                      textAlign: TextAlign.center,
                    ),
                  ]),
                ),
              ],
            ),
//            SizedBox(height: 20.0),
            Expanded(
              child: Scaffold(
                  backgroundColor: kPaperWhite,
                  body: Column(
                    children: <Widget>[
                      SizedBox(height: 20.0),
                      Text(
                        'SEARCH RESULT',
                        style: TextStyle(color: kSecondaryDarkColorBrown),
                      ),
                      SizedBox(height: 10.0),
//                      SizedBox(height: 20.0, child: C,),
                      Container(
                        height: 0.5,
//                        color: kDarkGrey,
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                              blurRadius: 1.0,
                              offset: Offset(4.0, 4.0),
                              color: Colors.black38)
                        ]),
                      ),
                      SizedBox(height: 10.0),
                      //TODO INSERT SEARCH RESULT FROM GOOGLEBOOKS
                      Expanded(
                        child: searchedBook(),
                      ),
                    ],
                  )),
            )
          ],
        ),
        bottomNavigationBar: ButtonBar(
          children: <Widget>[
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'CANCEL',
                  style: TextStyle(color: kSecondaryDarkColorBrown),
                )),
            RaisedButton(
                child: Text('CONFIRM'),
                onPressed: () {
                  Firestore.instance
                      .runTransaction((Transaction transaction) async {
                    final CollectionReference reference = Firestore.instance
                        .collection('users/${widget.uid}/library');

                    await reference.add({
                      'title': title,
                      'author': author,
                      'isbn13': isbn13,
                      'timestamp': DateTime.now(),
                      'list': true
                    });
                  });
                  Navigator.of(context).pop();
                }),
          ],
        ));
  }
}
