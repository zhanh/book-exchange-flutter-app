import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import './model/book.dart';
import 'colors.dart';

const double _kFlingVelocity = 2.0;

class DiscoverBackdrop extends StatefulWidget {
  final Widget frontLayer;
  final Widget backLayer;
  final Widget frontTitle;
  final Widget backTitle;

  const DiscoverBackdrop({
    Key key,
    @required this.frontLayer,
    @required this.backLayer,
    @required this.frontTitle,
    @required this.backTitle,
  })  : assert(frontLayer != null),
        assert(backLayer != null),
        assert(frontTitle != null),
        assert(backTitle != null);

  @override
  _DiscoverBackdropState createState() => _DiscoverBackdropState();
}

class _DiscoverBackdropState extends State<DiscoverBackdrop>
    with SingleTickerProviderStateMixin {
  final GlobalKey _backdropKey = GlobalKey(debugLabel: 'Backdrop');

  // TODO: Add AnimationController widget (104)
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: Duration(milliseconds: 300),
      value: 1.0,
      vsync: this,
    );
  }

  // TODO: Add override for didUpdateWidget (104)

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  // TODO: Add functions to get and change front layer visibility (104)
  bool get _frontLayerVisible {
    final AnimationStatus status = _controller.status;
    return status == AnimationStatus.completed ||
        status == AnimationStatus.forward;
  }

  void _toggleBackdropLayerVisibility() {
    _controller.fling(
        velocity: _frontLayerVisible ? -_kFlingVelocity : _kFlingVelocity);
  }

  // TODO: Add BuildContext and BoxConstraints parameters to _buildStack (104)
  Widget _buildStack(BuildContext context, BoxConstraints constraints) {
    const double layerTitleHeight = 48.0;
    final Size layerSize = constraints.biggest;
    final double layerTop = layerSize.height - layerTitleHeight;

    // TODO: Create a RelativeRectTween Animation (104)
    Animation<RelativeRect> layerAnimation = RelativeRectTween(
      begin: RelativeRect.fromLTRB(
          0.0, layerTop, 0.0, layerTop - layerSize.height),
      end: RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0),
    ).animate(_controller.view);

    return Stack(
      key: _backdropKey,
      children: <Widget>[
        SizedBox(
          height: 5.0,
          width: 5.0,
        ),
        widget.backLayer,
        PositionedTransition(
          rect: layerAnimation,
          child: _FrontLayer(child: widget.frontLayer),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var appBar = AppBar(
      backgroundColor: kPrimaryColorAmber,
      brightness: Brightness.light,
      elevation: 0.0,
      // TODO: Replace leading menu icon with IconButton (104)
      // TODO: Remove leading property (104)
      // TODO: Create title with _BackdropTitle parameter (104)
      leading: IconButton(
        icon: Icon(Icons.menu),
        onPressed: _toggleBackdropLayerVisibility,
      ),
      title: widget.frontTitle,
      actions: <Widget>[
        // TODO: Add shortcut to login screen from trailing icons (104)
        IconButton(
          icon: Icon(Icons.search),
          onPressed: () {
            // TODO: Add open login (104)
          },
        ),
      ],
    );
    return Scaffold(
      appBar: appBar,

      // TODO: Return a LayoutBuilder widget (104)
      body: LayoutBuilder(builder: _buildStack),
    );
  }
}

// TODO: Add _FrontLayer class (104)
class _FrontLayer extends StatelessWidget {
  // TODO: Add on-tap callback (104)
  const _FrontLayer({
    Key key,
    this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    var dotts = new Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        gradient: LinearGradient(
            begin: Alignment.topRight,
            end: new Alignment(12.0, 0.0),
            colors: [kPrimaryColorAmber, kDarkGrey]),
      ),
      height: 50.0,
      width: 10.0,
      margin: EdgeInsets.symmetric(horizontal: 5.0),
    );

    var dottsRow = Container(
      height: 50.0,
      alignment: Alignment.center,
      color: kPaperWhite,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          dotts,
          dotts,
          dotts,
        ],
      ),
    );

    var collectionReference = Firestore.instance.collection('books@exchange');
    var query = collectionReference.where('public', isEqualTo: true);

    return Material(
      elevation: 12.0,
      shadowColor: kDarkGrey,
      shape: BeveledRectangleBorder(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(24.0)),
      ),
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
            backgroundColor: kPaperWhite,
            appBar: TabBar(
                labelColor: kSecondaryDarkColorBrown,
                unselectedLabelColor: kPrimaryDarkColorBrown,
                indicatorColor: kPrimaryColorAmber,
                tabs: [
                  Tab(
                    child: Text(
                      'FEATURED',
                      style: TextStyle(fontSize: 12.0),
                    ),
                  ),
                  Tab(
                    child: Text(
                      'MOST FAVORITED',
                      style: TextStyle(fontSize: 12.0),
                    ),
                  ),
                  Tab(
                    child: Text(
                      'MOST RECENT',
                      style: TextStyle(fontSize: 12.0),
                    ),
                  ),
                ]),
            body: TabBarView(children: [
              StreamBuilder(
                stream: query.snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(
                        child: Container(
                      height: 65.0,
                      width: 55.0,
                      child: CircularProgressIndicator(),
                    ));
                  return FirestoreListView(documents: snapshot.data.documents);
                },
              ),
              StreamBuilder(
                stream: query.snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    Center(
                        child: Container(
                      height: 55.0,
                      width: 55.0,
                      child: CircularProgressIndicator(),
                    ));
                  return FirestoreListView(documents: snapshot.data.documents);
                },
              ),
              StreamBuilder(
                stream: query.snapshots(),
                builder: (context, snapshot) {
                  Center(
                      child: Container(
                    height: 65.0,
                    width: 55.0,
                    child: CircularProgressIndicator(),
                  ));
                  return FirestoreListView(documents: snapshot.data.documents);
                },
              )
            ])),
      ),
    );
  }
}

//gridview
class FirestoreListView extends StatelessWidget {
  final List<DocumentSnapshot> documents;

  FirestoreListView({this.documents});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print("IN BUILD");

    return GridView.builder(
        itemCount: documents.length,
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          String title = documents[index].data['title'].toString();
          String author = documents[index].data['author'].toString();
          String isbn13 = documents[index].data['isbn13'].toString();
//          List<Object> books = documents[index].data['library'];
//          print('isbn: ' + isbn);
//          List<dynamic> library = documents[index].data['library'];
          print("DOC LENGTH: ${documents.length}");
          print("title $title");
          print("author $author");
          print("isbn13 $isbn13");
          String thumbnailUrl;
//          print(title + ' ' + author);
//          Future<Book> fetchPost() async {
//            var uri = 'https://www.googleapis.com/books/v1/volumes?q=' + isbn;
//            print('uri: ' + uri);
//            final response = await http.get(uri);
//            if (response.statusCode == 200) {
//              return Book.fromJson(json.decode(response.body));
//            } else {
//              throw Exception('Error: Failed to load');
//            }
//          }
          Future<Book> fetchPost() async {
            var uri = 'https://www.googleapis.com/books/v1/volumes?q=' + isbn13;
//            print("FETCH POST USER: " + widget.auth.uid());
            print("fetching response");
            print('uri: ' + uri);
            final response = await http.get(uri);
            if (response.statusCode == 200) {
              return Book.fromJson(json.decode(response.body));
            } else {
              throw Exception('Error: Failed to load');
            }
          }

          return ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              title: Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                    color: kDarkGrey,
//                    gradient: LinearGradient(
//                        begin:
//
//                        Alignment.bottomRight,
//                        end: new Alignment(0.5, 0.0),
//                        colors: [
//                          kPrimaryColorAmber,
//                          kSecondaryDarkColorBrown,
//                        ]),
                    borderRadius: BorderRadius.circular(5.0),
//                    border: Border.all(color: kPaleWhite),
//                    color: kPrimaryDarkColorBrown,
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 1.0,
                          offset: Offset(4.0, 4.0),
                          color: Colors.black38)
                    ]),

                child: FutureBuilder<Book>(
                    future: fetchPost(),
                    builder:
                        (BuildContext context, AsyncSnapshot<Book> snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return SnackBar(
                            content: Row(
                              children: <Widget>[
                                Text(
                                    'No internet connection. Unable to load data.'),
                                FlatButton(
                                    onPressed: () {
                                      //TODO REFRESH PAGE FUNCTION
                                    },
                                    child: Text('TAP TO RETRY'))
                              ],
                            ),
                            duration: Duration(seconds: 5),
                          );
                        case ConnectionState.waiting:
                          return Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 65.0, vertical: 55.0),
                              child: CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    kPrimaryColorAmber),
                              ));
                          break;
                        default:
                          if (snapshot.hasError) {
                            print('Error: in error snapshot' + snapshot.error);
                            return new Container(
                              width: 140.0,
                              height: 160.0,
                              color: kprimaryLightColorAmber,
                              child: Icon(
                                Icons.broken_image,
                                color: kPrimaryDarkColorBrown,
                              ),
                            );
                          } else {
                            try {
                              thumbnailUrl = snapshot.data.thumbnailUrl;
                            } catch (e) {
                              print(e);
                            }
//                            print('image url: ' + thumbnailUrl.toString());
                            return Container(
                                decoration: BoxDecoration(
                                    border: Border.all(color: kPaperWhite)),
                                child: Image.network(
                                  thumbnailUrl,
                                  fit: BoxFit.contain,
                                ));
                          }
                      }
                    }),

//                    Expanded(
//                        child: Container(
//                      margin: EdgeInsets.fromLTRB(0.0, 0.0, 20.0, 12.0),
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        mainAxisAlignment: MainAxisAlignment.end,
//                        children: <Widget>[
//                          Text(
//                            title,
//                            softWrap: true,
//                            maxLines: 3,
//                            overflow: TextOverflow.ellipsis,
//                            style: TextStyle(
//                              fontWeight: FontWeight.w700,
//                            ),
//                          ),
//                          SizedBox(height: 4.0),
//                          Text(author,
//                              softWrap: true,
//                              maxLines: 2,
//                              overflow: TextOverflow.ellipsis,
//                              style: TextStyle(fontStyle: FontStyle.italic)),
//                        ],
//                      ),
//                    )),
              ));
        });
  }
}

//listview
//class FirestoreListView extends StatelessWidget {
//  final List<DocumentSnapshot> documents;
//
//  FirestoreListView({this.documents});
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    print("IN BUILD");
//    return ListView.builder(
//        itemCount: documents.length,
//        itemExtent: 170.0,
//        itemBuilder: (BuildContext context, int index) {
//          String title = documents[index].data['title'].toString();
//          String author = documents[index].data['author'].toString();
//          String isbn13 = documents[index].data['isbn13'].toString();
//
//          print("title $title");
//          print("author $author");
//          print("isbn13 $isbn13");
//
//          Future<Book> fetchPost() async {
//            var uri = 'https://www.googleapis.com/books/v1/volumes?q=' + isbn13;
//            print("fetching response");
//            print('uri: ' + uri);
//            final response = await http.get(uri);
//            if (response.statusCode == 200) {
//              return Book.fromJson(json.decode(response.body));
//            } else {
//              throw Exception('Error: Failed to load');
//            }
//          }
//
//          return ListTile(
//              contentPadding:
//                  EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
//              title: Container(
//                margin: EdgeInsets.symmetric(horizontal: 10.0),
//                decoration: BoxDecoration(
//                    gradient: LinearGradient(
//                        begin: Alignment.bottomRight,
//                        end: new Alignment(0.1, 0.0),
//                        colors: [
//                          kPrimaryDarkColorBrown,
//                          kSecondaryDarkColorBrown,
//                        ]),
//                    borderRadius: BorderRadius.circular(5.0),
//                    boxShadow: [
//                      BoxShadow(
//                          blurRadius: 1.0,
//                          offset: Offset(4.0, 4.0),
//                          color: Colors.black38)
//                    ]),
//                child: Row(
//                  children: <Widget>[
//                    Container(
//                      margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
//                      width: 108.0,
//                      color: kPrimaryDarkColorBrown,
//                      child: FutureBuilder<Book>(
//                          future: fetchPost(),
//                          builder: (BuildContext context,
//                              AsyncSnapshot<Book> snapshot) {
//                            switch (snapshot.connectionState) {
//                              case ConnectionState.none:
//                                return SnackBar(
//                                  content: Row(
//                                    children: <Widget>[
//                                      Text(
//                                          'No internet connection. Unable to load data.'),
//                                      FlatButton(
//                                          onPressed: () {
//                                            //TODO REFRESH PAGE FUNCTION
//                                          },
//                                          child: Text('TAP TO RETRY'))
//                                    ],
//                                  ),
//                                  duration: Duration(seconds: 5),
//                                );
//                              case ConnectionState.waiting:
//                                return Container(
//                                    padding: EdgeInsets.symmetric(
//                                        horizontal: 40.0, vertical: 55.0),
//                                    child: CircularProgressIndicator(
//                                      valueColor:
//                                          new AlwaysStoppedAnimation<Color>(
//                                              kPrimaryColorAmber),
//                                    ));
//                                break;
//                              default:
//                                if (snapshot.hasError) {
//                                  print('Error: in error snapshot' +
//                                      snapshot.error);
//                                  return new Container(
//                                    width: 140.0,
//                                    height: 160.0,
//                                    color: kprimaryLightColorAmber,
//                                    child: Icon(
//                                      Icons.broken_image,
//                                      color: kPrimaryDarkColorBrown,
//                                    ),
//                                  );
//                                } else {
//                                  var smallThumbnailUrl =
//                                      snapshot.data.smallThumbnailUrl;
//                                  print('image url: ' +
//                                      smallThumbnailUrl.toString());
//                                  return Container(
//                                      decoration: BoxDecoration(
//                                          border:
//                                              Border.all(color: kPaperWhite)),
//                                      child: Image.network(
//                                        smallThumbnailUrl,
//                                        fit: BoxFit.cover,
//                                      ));
//                                }
//                            }
//                          }),
//                    ),
//                    Expanded(
//                        child: Container(
//                      margin: EdgeInsets.fromLTRB(0.0, 0.0, 20.0, 12.0),
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        mainAxisAlignment: MainAxisAlignment.end,
//                        children: <Widget>[
//                          Text(
//                            title,
//                            softWrap: true,
//                            maxLines: 3,
//                            overflow: TextOverflow.ellipsis,
//                            style: TextStyle(
//                              fontWeight: FontWeight.w700,
//                            ),
//                          ),
//                          SizedBox(height: 4.0),
//                          Text(author,
//                              softWrap: true,
//                              maxLines: 2,
//                              overflow: TextOverflow.ellipsis,
//                              style: TextStyle(fontStyle: FontStyle.italic)),
//                        ],
//                      ),
//                    )),
//                  ],
//                ),
//              ));
//        });
//  }
//}
