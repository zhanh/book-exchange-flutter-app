import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import './AddBookPopup.dart';
import 'auth.dart';
import 'colors.dart';

const double _kFlingVelocity = 2.0;

class MyLibraryBackdrop extends StatefulWidget {
  final Widget frontLayer;
  final Widget backLayer;
  final Widget frontTitle;
  final Widget backTitle;
  final BaseAuth auth;

  const MyLibraryBackdrop({
    Key key,
    @required this.frontLayer,
    @required this.backLayer,
    @required this.frontTitle,
    @required this.backTitle,
    @required this.auth,
  })  : assert(frontLayer != null),
        assert(backLayer != null),
        assert(frontTitle != null),
        assert(backTitle != null),
        assert(auth != null);

  @override
  _MyLibraryBackdropState createState() => _MyLibraryBackdropState();
}

class _MyLibraryBackdropState extends State<MyLibraryBackdrop>
    with SingleTickerProviderStateMixin {
  final GlobalKey _backdropKey = GlobalKey(debugLabel: 'Backdrop');
  final GlobalKey _searchBookListKey = GlobalKey(debugLabel: 'Search List');
  AnimationController _controller;
  String _uid;

  @override
  void initState() {
    widget.auth.currentUser().then((userId) {
      setState(() {
        userId == null
            ? print("ERROR IN INIT STATE LIBRARY USERID NULL")
            : _uid = userId.uid.toString();
        print('USERID IN INIT STATE: $_uid');
      });
    });

    _controller = AnimationController(
      duration: Duration(milliseconds: 300),
      value: 1.0,
      vsync: this,
    );
    super.initState();
  }

  // TODO: Add override for didUpdateWidget (104)

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  // TODO: Add functions to get and change front layer visibility (104)
  bool get _frontLayerVisible {
    final AnimationStatus status = _controller.status;
    return status == AnimationStatus.completed ||
        status == AnimationStatus.forward;
  }

  void _toggleBackdropLayerVisibility() {
    _controller.fling(
        velocity: _frontLayerVisible ? -_kFlingVelocity : _kFlingVelocity);
  }

  // TODO: Add BuildContext and BoxConstraints parameters to _buildStack (104)
  Widget _buildStack(BuildContext context, BoxConstraints constraints) {
    const double layerTitleHeight = 48.0;
    final Size layerSize = constraints.biggest;
    final double layerTop = layerSize.height - layerTitleHeight;

    // TODO: Create a RelativeRectTween Animation (104)
    Animation<RelativeRect> layerAnimation = RelativeRectTween(
      begin: RelativeRect.fromLTRB(
          0.0, layerTop, 0.0, layerTop - layerSize.height),
      end: RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0),
    ).animate(_controller.view);

    return Stack(
      key: _backdropKey,
      children: <Widget>[
        SizedBox(
          height: 5.0,
          width: 5.0,
        ),
        widget.backLayer,
        PositionedTransition(
          rect: layerAnimation,
          child: _FrontLayer(child: widget.frontLayer),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var appBar = AppBar(
      backgroundColor: kPrimaryColorAmber,
      brightness: Brightness.light,
      elevation: 0.0,
      leading: IconButton(
        icon: Icon(Icons.menu),
        onPressed: _toggleBackdropLayerVisibility,
      ),
      title: widget.frontTitle,
      actions: <Widget>[
        // TODO: Add shortcut to login screen from trailing icons (104)
        IconButton(
            icon: Icon(Icons.add_box),
            onPressed: () {
              print('IN LIBRARY BACKDROP -- $_uid');

              Navigator.push(
                context,
                AddBookRoute(
                    widget: AddBookPopup(
                  uid: _uid,
                )),
//                  new PageRouteBuilder(
//                      opaque: false,
//                      pageBuilder: (BuildContext context, _, __) {
//                        return new Container(
//                          color: kPaperWhite,
//                          width: 250.0,
//                          height: 200.0,
//                          child: ButtonBar(
//                            children: <Widget>[
//                              FlatButton(
//                                  onPressed: () {}, child: Text('CANCEL')),
//                              RaisedButton(
//                                  child: Text('OK'),
//                                  onPressed: () {
//                                    Navigator.pop(context);
//                                  }),
//                            ],
//                          ),
//                        );
//                      },
//                      transitionsBuilder: (BuildContext context,
//                          Animation<double> animation,
//                          Animation<double> secondaryAnimation,
//                          Widget child) {
//                        return new ScaleTransition(
//                          scale: new Tween<double>(
//                            begin: 0.0,
//                            end: 1.0,
//                          ).animate(
//                            CurvedAnimation(
//                              parent: animation,
//                              curve: Interval(
//                                0.00,
//                                0.50,
//                                curve: Curves.linear,
//                              ),
//                            ),
//                          ),
//                          child: ScaleTransition(
//                            scale: Tween<double>(
//                              begin: 1.5,
//                              end: 1.0,
//                            ).animate(
//                              CurvedAnimation(
//                                parent: animation,
//                                curve: Interval(
//                                  0.50,
//                                  1.00,
//                                  curve: Curves.linear,
//                                ),
//                              ),
//                            ),
//                            child: child,
//                          ),
//                        );
//                      })
              );
            }),
      ],
    );
    return Scaffold(
      appBar: appBar,

      // TODO: Return a LayoutBuilder widget (104)
      body: LayoutBuilder(builder: _buildStack),
    );
  }
}

// TODO: Add _FrontLayer class (104)
class _FrontLayer extends StatelessWidget {
  // TODO: Add on-tap callback (104)
  const _FrontLayer({
    Key key,
    this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    var dotts = new Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        gradient: LinearGradient(
            begin: Alignment.topRight,
            end: new Alignment(12.0, 0.0),
            colors: [kPrimaryColorAmber, kDarkGrey]),
      ),
      height: 50.0,
      width: 10.0,
      margin: EdgeInsets.symmetric(horizontal: 5.0),
    );

    var dottsRow = Container(
      height: 50.0,
      alignment: Alignment.center,
      color: kPaperWhite,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          dotts,
          dotts,
          dotts,
        ],
      ),
    );

    return Material(
      elevation: 12.0,
      shadowColor: kDarkGrey,
      shape: BeveledRectangleBorder(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(24.0)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          dottsRow,
          // TODO: Add a GestureDetector (104)
          Expanded(
            child: child,
          ),
        ],
      ),
    );
  }
}

class AddBookRoute extends PageRouteBuilder {
  final Widget widget;

  AddBookRoute({this.widget})
      : super(pageBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secondaryAnimation) {
          return widget;
        }, transitionsBuilder: (BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child) {
          return new ScaleTransition(
            scale: new Tween<double>(
              begin: 0.0,
              end: 1.0,
            ).animate(
              CurvedAnimation(
                parent: animation,
                curve: Interval(
                  0.00,
                  0.50,
                  curve: Curves.linear,
                ),
              ),
            ),
            child: ScaleTransition(
              scale: Tween<double>(
                begin: 1.5,
                end: 1.0,
              ).animate(
                CurvedAnimation(
                  parent: animation,
                  curve: Interval(
                    0.50,
                    1.00,
                    curve: Curves.linear,
                  ),
                ),
              ),
              child: child,
            ),
          );
        });
}

List<String> fileNames = <String>[
  'assets/profile-pic-1.jpg',
  'assets/book-image-1.jpg',
  'assets/profile-pic-2.jpg',
];

class FirestoreListView extends StatelessWidget {
  final List<DocumentSnapshot> documents;

  FirestoreListView({this.documents});

  @override
  Widget build(BuildContext context) {
//    print("IN BUILD");
    return ListView.builder(
        itemCount: documents.length,
        itemExtent: 120.0,
        itemBuilder: (BuildContext context, int index) {
          String title = documents[index].data['title'].toString();
          String author = documents[index].data['author'].toString();
          String isbn13 = documents[index].data['isbn13'].toString();

//          print("title $title");
//          print("author $author");
//          print("isbn13 $isbn13");

//          Future<Book> fetchPost() async {
//            var uri = 'https://www.googleapis.com/books/v1/volumes?q=' + isbn13;
//            print("fetching response");
//            print('uri: ' + uri);
//            final response = await http.get(uri);
//            if (response.statusCode == 200) {
//              return Book.fromJson(json.decode(response.body));
//            } else {
//              throw Exception('Error: Failed to load');
//            }
//          }

          return ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              title: Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(
                      color: kDarkGrey, width: 0.2, style: BorderStyle.solid),
                )),
//                decoration: BoxDecoration(
//                    gradient: LinearGradient(
//                        begin: Alignment.bottomRight,
//                        end: new Alignment(0.1, 0.0),
//                        colors: [
//                          kPrimaryDarkColorBrown,
//                          kSecondaryDarkColorBrown,
//                        ]),
//                    borderRadius: BorderRadius.circular(5.0),
//                    boxShadow: [
//                      BoxShadow(
//                          blurRadius: 1.0,
//                          offset: Offset(4.0, 4.0),
//                          color: Colors.black38)
//                    ]),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Container(
                      margin: EdgeInsets.fromLTRB(0.0, 0.0, 20.0, 12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            title,
                            softWrap: true,
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: kSecondaryDarkColorBrown,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(height: 4.0),
                          Text(author,
                              softWrap: true,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: kSecondaryDarkColorBrown,
                                  fontStyle: FontStyle.italic)),
                          SizedBox(height: 4.0),
                          Text(isbn13,
                              softWrap: true,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: kSecondaryDarkColorBrown,
                                  fontStyle: FontStyle.italic)),
                        ],
                      ),
                    )),
                  ],
                ),
              ));
        });
  }
}

class SearchBook extends StatefulWidget {
  @override
  SearchBookState createState() {
    return new SearchBookState();
  }
}

class SearchBookState extends State<SearchBook> {
  final myController = TextEditingController();

  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('SEARCH BOOK'),
      ),
      body: Center(
          child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              TextField(
                controller: myController,
//                  onSubmitted: (){
//                    //TODO run the search
//                  },
              )
            ],
          )
        ],
      )),
    );
  }
}

//          actions: <Widget>[
//            IconButton(
//              icon: Icon(
//                Icons.center_focus_strong,
//                color: kPaperWhite,
//              ),
//              onPressed: () {
////                Navigator.push(
////                  context,
////                  MaterialPageRoute(builder: (context) => BarcodeScan()),
////                );
//              },
//            ),
//            IconButton(
//                icon: Icon(
//                  Icons.search,
//                  color: kPaperWhite,
//                ),
//                onPressed: () {
////                  return _searchBook();
//                  return showDialog(
//                      context: context,
//                      builder: (context) {
////                        return AlertDialog(
////                          content: Text(myController.text),
////                        );
//
//                        return AlertDialog(
//                            content: Column(
//                          mainAxisAlignment: MainAxisAlignment.spaceAround,
//                          crossAxisAlignment: CrossAxisAlignment.center,
//                          children: <Widget>[
//                            Text("SEARCH BOOK"),
//                            SizedBox(
//                              height: 20.0,
//                            ),
//                            TextField(
//                              decoration: InputDecoration(
//                                icon: Icon(Icons.search),
//                                labelText: 'TYPE ISBN',
//                              ),
//                              controller: myController,
//                            ),
//                            Divider(
//                              height: 2.0,
//                              color: kPaperWhite,
//                            ),
//                            RaisedButton(
//                                child: Text('SCAN BARCODE'),
//                                color: kPrimaryColorAmber,
//                                onPressed: () {
//                                  //TODO INSERT ML KIT PAGE,
//                                }),
//                            ButtonBar(
//                              children: <Widget>[
//                                FlatButton(
//                                  child: Text('CANCEL',
//                                      style: TextStyle(color: kPaperWhite)),
//                                  onPressed: () {
//                                    setState(() {});
//                                  },
//                                ),
//                                RaisedButton(
//                                  color: kPrimaryColorAmber,
//                                  child: Text('SEARCH',
//                                      style: TextStyle(
//                                          color: kSecondaryDarkColorBrown)),
//                                  onPressed: () {},
//                                ),
//                              ],
//                            ),
//                          ],
//                        ));
//                      });
//                }),
//          ],
