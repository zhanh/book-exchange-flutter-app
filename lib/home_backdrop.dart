import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import 'colors.dart';

const double _kFlingVelocity = 2.0;

class HomeBackdrop extends StatefulWidget {
  final Widget frontLayer;
  final Widget backLayer;
  final Widget frontTitle;
  final Widget backTitle;

  const HomeBackdrop({
    Key key,
    @required this.frontLayer,
    @required this.backLayer,
    @required this.frontTitle,
    @required this.backTitle,
  })  : assert(frontLayer != null),
        assert(backLayer != null),
        assert(frontTitle != null),
        assert(backTitle != null);

  @override
  _HomeBackdropState createState() => _HomeBackdropState();
}

class _HomeBackdropState extends State<HomeBackdrop>
    with
        SingleTickerProviderStateMixin,
        AutomaticKeepAliveClientMixin<HomeBackdrop> {
  final GlobalKey _backdropKey = GlobalKey(debugLabel: 'Backdrop');

  // TODO: Add AnimationController widget (104)
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: Duration(milliseconds: 300),
      value: 1.0,
      vsync: this,
    );
  }

  // TODO: Add override for didUpdateWidget (104)

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  // TODO: Add functions to get and change front layer visibility (104)
  bool get _frontLayerVisible {
    final AnimationStatus status = _controller.status;
    return status == AnimationStatus.completed ||
        status == AnimationStatus.forward;
  }

  void _toggleBackdropLayerVisibility() {
    _controller.fling(
        velocity: _frontLayerVisible ? -_kFlingVelocity : _kFlingVelocity);
  }

  // TODO: Add BuildContext and BoxConstraints parameters to _buildStack (104)
  Widget _buildStack(BuildContext context, BoxConstraints constraints) {
    const double layerTitleHeight = 48.0;
    final Size layerSize = constraints.biggest;
    final double layerTop = layerSize.height - layerTitleHeight;

    // TODO: Create a RelativeRectTween Animation (104)
    Animation<RelativeRect> layerAnimation = RelativeRectTween(
      begin: RelativeRect.fromLTRB(
          0.0, layerTop, 0.0, layerTop - layerSize.height),
      end: RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0),
    ).animate(_controller.view);

    return Stack(
      key: _backdropKey,
      children: <Widget>[
        SizedBox(
          height: 5.0,
          width: 5.0,
        ),
        widget.backLayer,
        PositionedTransition(
          rect: layerAnimation,
          child: _FrontLayer(child: widget.frontLayer),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var appBar = AppBar(
      backgroundColor: kPrimaryColorAmber,
      brightness: Brightness.light,
      elevation: 0.0,
      // TODO: Replace leading menu icon with IconButton (104)
      // TODO: Remove leading property (104)
      // TODO: Create title with _BackdropTitle parameter (104)
      leading: IconButton(
        icon: Icon(Icons.menu),
        onPressed: _toggleBackdropLayerVisibility,
      ),
      title: widget.frontTitle,
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.settings),
          onPressed: () {
            // TODO: Add open login (104)
          },
        ),
      ],
    );
    return Scaffold(
      appBar: appBar,

      // TODO: Return a LayoutBuilder widget (104)
      body: LayoutBuilder(builder: _buildStack),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

// TODO: Add _FrontLayer class (104)
class _FrontLayer extends StatelessWidget {
  // TODO: Add on-tap callback (104)
  const _FrontLayer({
    Key key,
    this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    var dotts = new Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        gradient: LinearGradient(
            begin: Alignment.topRight,
            end: new Alignment(12.0, 0.0),
            colors: [kPrimaryColorAmber, kDarkGrey]),
      ),
      height: 50.0,
      width: 10.0,
      margin: EdgeInsets.symmetric(horizontal: 5.0),
    );

    var dottsRow = Container(
      height: 50.0,
      alignment: Alignment.center,
      color: kPaperWhite,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          dotts,
          dotts,
          dotts,
        ],
      ),
    );

    return Material(
      elevation: 12.0,
      shadowColor: kDarkGrey,
      shape: BeveledRectangleBorder(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(24.0)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          dottsRow,
          // TODO: Add a GestureDetector (104)
          Expanded(
            child: child,
          ),
        ],
      ),
    );
  }
}

//Row(
//crossAxisAlignment: CrossAxisAlignment.center,
//children: <Widget>[
//new Container(
//height: 50.0,
//width: 10.0,
////            margin: EdgeInsets.all(25.0),
////            padding: EdgeInsets.all(),
////            margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
//padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
//color: Colors.white,
//child: new Container(
//decoration: new BoxDecoration(
//color: kPrimaryColorAmber,
//shape: BoxShape.circle,
//),
//),
//),
//new Container(
//height: 50.0,
//width: 10.0,
////            margin: EdgeInsets.all(25.0),
////            padding: EdgeInsets.all(),
////            margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
//padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
//color: Colors.white,
//child: new Container(
//decoration: new BoxDecoration(
//color: kPrimaryColorAmber,
//shape: BoxShape.circle,
//),
//),
//),
//new Container(
//height: 50.0,
//width: 10.0,
////            margin: EdgeInsets.all(25.0),
////            padding: EdgeInsets.all(),
////            margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
//padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
//color: Colors.white,
//child: new Container(
//decoration: new BoxDecoration(
//color: kPrimaryColorAmber,
//shape: BoxShape.circle,
//),
//),
//),
//],
//),
